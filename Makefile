builderDir := builder
cliDir := cli
binary := antibuild
outbinary := $(binary)
debugflags = -gcflags=all="-N -l" 

branch = $(shell git rev-parse --abbrev-ref HEAD)

$(binary): $(shell find . -name '*.go' -type f)
	go build -ldflags="-s -w" -o $(outbinary) cmd/*.go

clean:
	-rm $(binary)

build: build_darwin build_linux build_windows

build_darwin:
	export GOOS=darwin; \
		make build_amd64;

build_linux:
	export GOOS=linux; \
		make build_amd64; \
		make build_386; \
		make build_arm64; \
		make build_arm;

build_windows:
	export GOOS=windows; \
		make build_amd64; \
		make build_386;

build_amd64:
	export GOARCH=amd64; \
		make build_internal;

build_386:
	export GOARCH=386; \
		make build_internal;

build_arm64:
	export GOARCH=amd64; \
		make build_internal;

build_arm:
	export GOARCH=amd64; \
		make build_internal;

build_internal:
	echo "Building antibuild for ${GOOS}/${GOARCH}";
	go build -ldflags="-s -w" -o ./dist/${GOOS}/${GOARCH}/antibuild cmd/*.go

test:
	cd engine; go test ./...

benchcmp: $(shell which benchcmp)
	go get golang.org/x/tools/cmd/benchcmp 

benchmark: benchcmp
	go mod download
	go test ./... -bench=. -test.benchmem=true -run=^$ > benchmark.txt; true
	-wget "https://gitlab.com/antipy/antibuild/cli/-/jobs/artifacts/$(branch)/raw/benchmark.txt?job=benchmark" -O benchmark_before.txt; true
	-benchcmp benchmark_before.txt benchmark.txt > benchmark_change.txt; true


update_std:
	go get -d gitlab.com/antipy/antibuild/std/file@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/json@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/language@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/markdown@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/math@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/util@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/yaml@${VERSION}
	go get -d gitlab.com/antipy/antibuild/std/deno@${VERSION}
	