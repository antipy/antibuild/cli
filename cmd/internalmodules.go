// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"github.com/blang/semver"
	"github.com/spf13/cobra"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	abm_deno "gitlab.com/antipy/antibuild/std/deno/handler"
	abm_file "gitlab.com/antipy/antibuild/std/file/handler"
	abm_json "gitlab.com/antipy/antibuild/std/json/handler"
	abm_language "gitlab.com/antipy/antibuild/std/language/handler"
	abm_markdown "gitlab.com/antipy/antibuild/std/markdown/handler"
	abm_math "gitlab.com/antipy/antibuild/std/math/handler"
	abm_util "gitlab.com/antipy/antibuild/std/util/handler"
	abm_yaml "gitlab.com/antipy/antibuild/std/yaml/handler"
)

var availableInternalModules = map[string]modules.InternalModule{
	"deno": {
		Start:      abm_deno.Handler,
		Version:    semver.MustParse(abm_deno.Version),
		Repository: modules.StandardRepository,
	},
	"file": {
		Start:      abm_file.Handler,
		Version:    semver.MustParse(abm_file.Version),
		Repository: modules.StandardRepository,
	},
	"json": {
		Start:      abm_json.Handler,
		Version:    semver.MustParse(abm_json.Version),
		Repository: modules.StandardRepository,
	},
	"language": {
		Start:      abm_language.Handler,
		Version:    semver.MustParse(abm_language.Version),
		Repository: modules.StandardRepository,
	},
	"markdown": {
		Start:      abm_markdown.Handler,
		Version:    semver.MustParse(abm_markdown.Version),
		Repository: modules.StandardRepository,
	},
	"math": {
		Start:      abm_math.Handler,
		Version:    semver.MustParse(abm_math.Version),
		Repository: modules.StandardRepository,
	},
	"util": {
		Start:      abm_util.Handler,
		Version:    semver.MustParse(abm_util.Version),
		Repository: modules.StandardRepository,
	},
	"yaml": {
		Start:      abm_yaml.Handler,
		Version:    semver.MustParse(abm_yaml.Version),
		Repository: modules.StandardRepository,
	},
}

func setupInternalModules(command *cobra.Command, args []string) {
	modules.InternalModules.Set(availableInternalModules) //ignore error. this will be the first time usage and it only errors on second time
}
