// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"github.com/spf13/cobra"
	cmdmodules "gitlab.com/antipy/antibuild/cli/cmd/modules"
	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

// addCommandRun is the cobra command
func addCommandRun(command *cobra.Command, args []string) {
	configfile := getString(command, configFlag.Name, configFlag.Value)
	repositoryFile := getString(command, modulesrepoFlag.Name, modulesrepoFlag.Value)
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfg, err := localConfig.GetDirtyConfig(configfile)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	cmdmodules.AddModule(cfg, args[0], repositoryFile, log)
	err = cfg.Save()
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	ui.PrintGeneric(log)
}

// removeCommandRun is the cobra command
func removeCommandRun(command *cobra.Command, args []string) {
	configfile := getString(command, configFlag.Name, configFlag.Value)
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfg, err := localConfig.GetDirtyConfig(configfile)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	module := args[0]

	err = cmdmodules.RemoveModule(cfg, module, log)
	if err != nil {
		log.Fatale(err)
	}
	err = cfg.Save()
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	log.Infof("Removed the module %s", module)
	ui.PrintGeneric(log)
}

// installCommandRun is the cobra command
func installCommandRun(command *cobra.Command, args []string) {
	configfile := getString(command, configFlag.Name, configFlag.Value)
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfg, err := localConfig.GetDirtyConfig(configfile)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	bar := ui.Loader{}
	bar.Total = len(cfg.Modules.Dependencies)
	i := 0

	for moduleName, module := range cfg.Modules.Dependencies {
		log.Infof("Installing %s from repository %s", module.Name, module.Repository)
		bar.Update(i, log)

		installedModule, err := cmdmodules.InstallModule(module, cfg.Folders.Modules, log)
		if err != nil {
			log.Fatale(err)
			ui.PrintGeneric(log)
			return
		}

		cfg.Modules.Dependencies[moduleName] = installedModule

		log.Infof("Installed %s", module.Name)
		i++
	}
	err = cfg.Save()
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	ui.PrintGeneric(log)
}
