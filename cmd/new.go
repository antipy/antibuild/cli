// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"io"
	"os"
	"path/filepath"
	"regexp"

	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	cmderrors "gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/cmd/templates"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type newAnswers struct {
	Name     string `survey:"name"`
	Template string `survey:"template"`
}

const defaultTemplateRepositoryURL = "https://build.antipy.com/dl/templates.json"
const defaultTemplateBranch = "master"

var (
	nameregex = regexp.MustCompile("[a-z-]{3,}")
)

func newCommandRun(command *cobra.Command, args []string) {
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)
	templateRepositoryURL := getString(command, temlaterepoFlag.Name, temlaterepoFlag.Value)
	templateBranch := getString(command, templatebranchFlag.Name, templatebranchFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	templateRepository, err := templates.GetRepository(templateRepositoryURL)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	templateOptions := make([]string, 0, len(templateRepository))

	for template := range templateRepository {
		templateOptions = append(templateOptions, template)
	}

	newProjectSurvey := []*survey.Question{
		{
			Name:     "name",
			Prompt:   &survey.Input{Message: "What should the name of the project be?"},
			Validate: nameValidator,
		},
		{
			Name: "template",
			Prompt: &survey.Select{
				Message: "Choose a starting template:",
				Options: templateOptions,
			},
		},
	}

	answers := newAnswers{}

	err = survey.Ask(newProjectSurvey, &answers)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}
	path, _ := filepath.Abs(answers.Name)
	if _, err = os.Stat(path); !os.IsNotExist(err) {
		f, err := os.Open(path)
		if err != nil {
			log.Fatale(err)
			ui.PrintGeneric(log)
			return
		}
		defer f.Close()
		_, err = f.Readdirnames(1) // Or f.Readdir(1)
		if err != io.EOF {
			if err != nil {
				log.Fatale(err)
			} else {
				log.Fatal("Target directory is not empty")
			}
			ui.PrintGeneric(log)
			return
		}
	}

	err = templates.Download(templateRepository, answers.Template, answers.Name, templateBranch)
	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	err = filepath.Walk(path, func(name string, info os.FileInfo, err error) error {
		if err == nil {
			err = os.Chmod(name, 0740)
		}
		return err
	})

	if err != nil {
		log.Fatale(err)
		ui.PrintGeneric(log)
		return
	}

	log.Info("Run these commands to get started:\n" +
		"cd " + answers.Name + "\n\n" +
		"antibuild develop\n\n" +
		"Need help? Look at our docs: https://build.antipy.com/get-started\n")
	ui.PrintGeneric(log)
}

func nameValidator(input interface{}) error {
	var in string
	var ok bool
	if in, ok = input.(string); !ok {
		return cmderrors.ErrInvalidInputType{
			Field: "name",
			Type:  "string",
		}
	}

	match := nameregex.MatchString(in)

	if !match {
		return cmderrors.ErrInvalidName{
			Name: in,
		}
	}
	return nil
}
