package ui

import (
	tm "github.com/buger/goterm"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	// Builder is so that is satisfies the resulter type in the Engine
	Builder struct {
		Log *logger.Logger
	}
)

// Update prints the build result state
func (b *Builder) Update() {
	PrintBuild(b.Log)
}

// PrintBuild prints the build result state
func PrintBuild(l *logger.Logger) {
	reset()

	switch l.MaxMessageType {
	case logger.FATAL:
		tm.Println(tm.Bold(tm.Color("Failed to build.", tm.RED)))
	case logger.WARNING:
		tm.Println(tm.Bold(tm.Color("Built with warnings.", tm.YELLOW)))
	default:
		tm.Println(tm.Bold(tm.Color("Built successfully.", tm.GREEN)))
		tm.Println()
		tm.Println("The " + tm.Color(tm.Bold("build"), tm.BLUE) + " folder is ready to be deployed.\n" +
			"You should serve it with a static hosting server.\n")
	}

	printMessages(l)
	l.Messages = []logger.Message{}
	l.MaxMessageType = logger.DEBUG
	tm.FlushOnlyVisible()
}
