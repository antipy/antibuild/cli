package ui

import (
	tm "github.com/buger/goterm"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

// PrintGeneric message
func PrintGeneric(l *logger.Logger) {
	reset()
	switch l.MaxMessageType {
	case logger.FATAL:
		tm.Println(tm.Bold(tm.Color("Failed to complete.", tm.RED)))
	case logger.WARNING:
		tm.Println(tm.Bold(tm.Color("Completed with warnings.", tm.YELLOW)))
	default:
		tm.Println(tm.Bold(tm.Color("Completed successfully.", tm.GREEN)))
	}

	printMessages(l)
	l.Messages = []logger.Message{}
	l.MaxMessageType = logger.DEBUG

	tm.FlushOnlyVisible()
}

// PrintList of strings
func PrintList(list []string) {
	reset()
	for _, item := range list {
		tm.Println(item)
	}

	tm.FlushOnlyVisible()
}
