package ui

import (
	"fmt"

	tm "github.com/buger/goterm"
	cmderrors "gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

// FormatMessage takes a message and a formatter and returns the formatted version of the given message
func FormatMessage(m logger.Message, formatter func(e error) string) string {
	switch m.Type {
	case logger.DEBUG:
		return tm.Color(fmt.Sprintf("%s\t %s", tm.Bold("debug"), FormatMessageContents(m, formatter)), tm.BLUE)
	case logger.INFO:
		return tm.Color(fmt.Sprintf("%s\t %s", tm.Bold("info"), FormatMessageContents(m, formatter)), tm.WHITE)
	case logger.WARNING:
		return tm.Color(fmt.Sprintf("%s\t %s", tm.Bold("warn"), FormatMessageContents(m, formatter)), tm.YELLOW)
	case logger.FATAL:
		return tm.Color(fmt.Sprintf("%s\t %s", tm.Bold("fatal"), FormatMessageContents(m, formatter)), tm.RED)
	}

	return tm.Color(fmt.Sprintf("[UNKOWN] %s", FormatMessageContents(m, formatter)), tm.MAGENTA)
}

// FormatMessageContents formats a message using a formatter if it contains an error and uses the plain message otherwise
func FormatMessageContents(m logger.Message, formatter func(e error) string) string {
	if m.Error != nil {
		return formatter(m.Error)
	}
	return m.Message
}

// FormatError takes an error and creates a good looking and more informative error message
func FormatError(e error) string {
	switch err := e.(type) {
	case errors.ErrFailedOpenFile:
		return fmt.Sprintf("Failed to open file at location '%s'.", err.FileLocation)
	case errors.ErrFailedCreateFile:
		return fmt.Sprintf("Failed to create file at location '%s'.", err.FileLocation)
	case errors.ErrFailedCreateFolder:
		return fmt.Sprintf("Failed to create a file or folder at '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case errors.ErrFailedRemoveFile:
		return fmt.Sprintf("Failed to remove file at location '%s'.", err.FileLocation)
	case errors.ErrFileDoesntExist:
		return fmt.Sprintf("Tried to open file that does not exist '%s'.", err.FileLocation)
	case errors.ErrModuleNotExistInternal:
		return fmt.Sprintf("The module '%s@%s' tried to load as an internal module but doesn't exist internally.", err.Identifier, err.Version)
	case errors.ErrModuleStringParseFailed:
		return fmt.Sprintf("The module string is not formatted correctly ('%s') so it can not be parsed.", err.String)
	case errors.ErrIteratorStringParseFailed:
		return fmt.Sprintf("The iterator string is not formatted correctly ('%s') so it can not be parsed.", err.String)
	case errors.ErrModuleFailedToStart:
		return fmt.Sprintf("The module '%s@%s' failed to start. This error might help: \n    %s", err.Identifier, err.Version, err.DetailedError.Error())
	case errors.ErrModuleFailedToObtainConnection:
		return fmt.Sprintf("The module '%s@%s' failed to connect to antibuild. The pipe '%s' failed. This error might help: \n    %s", err.Identifier, err.Version, err.PipeName, err.DetailedError.Error())
	case errors.ErrModuleFailedToObtainFunctions:
		return fmt.Sprintf("The module '%s@%s' failed to communicate its registered functions to antibuild. This error might help: \n    %s", err.Identifier, err.Version, err.DetailedError.Error())
	case errors.ErrModuleFailedToConfigure:
		return fmt.Sprintf("The module '%s@%s' failed to recieve its configuration from antibuild. This error might help: \n    %s", err.Identifier, err.Version, err.DetailedError.Error())
	case errors.ErrModuleFailedToExececutePipe:
		return fmt.Sprintf("The module '%s@%s' failed to execute a pipe (instruction). The pipe '%s' failed. This error might help: \n    %s", err.Identifier, err.Version, err.Pipe, err.DetailedError.Error())
	case errors.ErrModuleFunctionNotRegistered:
		return fmt.Sprintf("The module '%s@%s' does not have a function called '%s'.", err.Identifier, err.Version, err.Name)
	case errors.ErrModuleDataLoaderNotRegistered:
		return fmt.Sprintf("The module '%s@%s' does not have a data loader called '%s'.", err.Identifier, err.Version, err.Name)
	case errors.ErrModuleDataParserNotRegistered:
		return fmt.Sprintf("The module '%s@%s' does not have a data parser called '%s'.", err.Identifier, err.Version, err.Name)
	case errors.ErrModuleDataPostProcessorNotRegistered:
		return fmt.Sprintf("The module '%s@%s' does not have a data post processor called '%s'.", err.Identifier, err.Version, err.Name)
	case errors.ErrFailedToParseDataString:
		return fmt.Sprintf("The data string '%s' could not be parsed. Error at '%s'.", err.DataString, err.Location)
	case errors.ErrDataStringMissingRangeVariable:
		return fmt.Sprintf("The data string '%s' is missing an iterator for the range command.", err.DataString)
	case errors.ErrDataStringMissingIterator:
		return fmt.Sprintf("The data string '%s' is using the iterator '%s' that is not defined.", err.DataString, err.Iterator)
	case errors.ErrFailedCacheInvalidation:
		return "Failed to do cache invalidation check."
	case errors.ErrConfigFailedOpen:
		return fmt.Sprintf("Failed to open config file at path '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case errors.ErrConfigFailedParse:
		return fmt.Sprintf("Failed to parse config file. This error might help: \n    %s", err.DetailedError.Error())
	case errors.ErrConfigFailedWrite:
		return fmt.Sprintf("Failed to write to config file at path '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case errors.ErrNoTemplateFolderSpecified:
		return "No template folder was specified. Please specify one in the config."
	case errors.ErrNoOutputFolderSpecified:
		return "No output folder was specified. Please specify one in the config."
	case errors.ErrFailedToCreateLogfile:
		return fmt.Sprintf("Failed to create log file at path '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case errors.ErrFailedToExecuteTemplate:
		return fmt.Sprintf("The template '%s' could not be executed. This error might help: \n    %s", err.OutputPath, err.DetailedError.Error())
	case errors.ErrFailedToMoveStatic:
		return fmt.Sprintf("Failed to move the static folder. This error might help: \n    %s", err.DetailedError.Error())
	case errors.ErrUsingUnloadedModule:
		return fmt.Sprintf("Using a module '%s' which is unknown/unloaded.", err.Identifier)
	case errors.ErrFailedToStartFileWatcher:
		return fmt.Sprintf("The file watcher for '%s' could not be started. This error might help: \n    %s", err.Identifier, err.DetailedError.Error())
	case errors.ErrFileWatcherCrashed:
		return fmt.Sprintf("The file watcher for '%s' crashed. This error might help: \n    %s", err.Identifier, err.DetailedError.Error())
	case errors.ErrFailedToStartKeyboardListener:
		return fmt.Sprintf("The keyboard listener could not be started. This error might help: \n    %s", err.DetailedError.Error())
	case errors.ErrModuleStringParseVersionFailed:
		return fmt.Sprintf("Failed to parse the version in the module string (%s). This error might help: \n    %s", err.String, err.DetailedError.Error())
	case errors.ErrPortAlreadyInUse:
		return fmt.Sprintf("Could not start the hosting server as the port %s is already in use. Please try to host on a different port.", err.Port)
	case errors.ErrHosting:
		return fmt.Sprintf("The server hosting at port %s failed. This error might help: \n    %s", err.Port, err.DetailedError.Error())
	}

	return e.Error()
}

// FormatErrorCMD does the same as FormatError, but for error messages from the CMD
func FormatErrorCMD(e error) string {
	switch err := e.(type) {
	case cmderrors.ErrFailedModuleRepositoryDownload:
		return fmt.Sprintf("Failed to download the module repository '%s'. This error might help: \n    %s", err.RepositoryURL, err.DetailedError.Error())
	case cmderrors.ErrNotExist:
		return fmt.Sprintf("Module '%s@%s' does not exist in repository '%s'.", err.Identifier, err.Version, err.Repository)
	case cmderrors.ErrNoLatestSpecified:
		return fmt.Sprintf("Repository '%s' does not specify a 'latest' version for module '%s'.", err.Repository, err.Identifier)
	case cmderrors.ErrFailedModuleBinaryDownload:
		return fmt.Sprintf("Module '%s@%s' (repository '%s') failed to download binary from url '%s'. This error might help: \n    %s", err.Identifier, err.Version, err.Repository, err.BinaryURL, err.DetailedError.Error())
	case cmderrors.ErrUnkownSourceRepositoryType:
		return fmt.Sprintf("Module '%s@%s' (repository '%s') does uses source repository type '%s'. This source repository type is not supported in this version of antibuild.", err.Identifier, err.Version, err.Repository, err.SourceType)
	case cmderrors.ErrFailedGitRepositoryDownload:
		return fmt.Sprintf("Module '%s@%s' (repository '%s') failed to download source from git. This error might help: \n    %s", err.Identifier, err.Version, err.Repository, err.DetailedError.Error())
	case cmderrors.ErrFailedModuleBuild:
		return fmt.Sprintf("Module '%s@%s' (repository '%s') failed to to build from source. This error might help: \n    %s", err.Identifier, err.Version, err.Repository, err.DetailedError.Error())
	case cmderrors.ErrGlobalConfigFailedOpen:
		return fmt.Sprintf("Failed to open global config file at path '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case cmderrors.ErrGlobalConfigFailedParse:
		return fmt.Sprintf("Failed to parse global config file. This error might help: \n    %s", err.DetailedError.Error())
	case cmderrors.ErrGlobalConfigFailedWrite:
		return fmt.Sprintf("Failed to write to global config file at path '%s'. This error might help: \n    %s", err.Path, err.DetailedError.Error())
	case cmderrors.ErrRepositoryAlreadyInGlobalConfig:
		return fmt.Sprintf("Repository '%s' is already in the global config.", err.Repository)
	case cmderrors.ErrRepositoryNotInGlobalConfig:
		return fmt.Sprintf("Repository '%s' is not in the global config.", err.Repository)
	case cmderrors.ErrInvalidInputType:
		return fmt.Sprintf("The type for the field '%s' is not '%s'.", err.Field, err.Type)
	case cmderrors.ErrInvalidName:
		return fmt.Sprintf("The name '%s' is not valid. The name should be at least 3 characters long and only include a-z and -.", err.Name)
	case cmderrors.ErrModuleNotInConfig:
		return fmt.Sprintf("Module '%s' is not in the config.", err.Identifier)
	case cmderrors.ErrFailedTemplateRepositoryDownload:
		return fmt.Sprintf("Failed to download the template repository '%s'. This error might help: \n    %s", err.RepositoryURL, err.DetailedError.Error())
	case cmderrors.ErrTemplateNotAvailableInRepository:
		return fmt.Sprintf("Template '%s' does not exist in repository '%s'.", err.Identifier, err.Repository)
	case errors.ErrIteratorStringParsing:
		return fmt.Sprintf("The variable parsing has failed. There are too many %s in the input.", err.OverPopulated)
	case errors.ErrFailedModuleExecution:
		return fmt.Sprintf("The method %s does not exist in %s: %v", err.ModuleMethod, err.ModuleName, err.DetailedError)
	case errors.ErrFailedLoadingModules:
		if err.IsInternal {
			return fmt.Sprintf("Failed to load internal module %s at version %s. This should never happen. Please contact the developer(s) and file a bug report.", err.Module, err.Version)
		}
		return fmt.Sprintf("Failed to load the %s module at version %s.", err.Module, err.Version)
	}

	return FormatError(e)
}
