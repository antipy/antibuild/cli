package ui

import (
	"net"

	tm "github.com/buger/goterm"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	//Develop is a struct containing all the info needed for develop printing
	Develop struct {
		Log  *logger.Logger
		Port string
	}
)

// Update prints the develop state using the logger and port
func (d *Develop) Update() {
	reset()

	switch d.Log.MaxMessageType {
	case logger.FATAL:
		tm.Println(tm.Bold(tm.Color("Failed to build.", tm.RED)))
	case logger.WARNING:
		tm.Println(tm.Bold(tm.Color("Built with warnings.", tm.YELLOW)))
	default:
		tm.Println(tm.Bold(tm.Color("Built successfully.", tm.GREEN)))
		tm.Println()
		tm.Println("You can now view your project in the browser. \n" +
			"   Local:            http://localhost:" + d.Port + "/\n" +
			"   On Your Network:  http://" + getIP() + ":" + d.Port + "/\n" +
			"\n" +
			"Note that the development build is not optimized. \n" +
			"To create a production build, use " + tm.Color(tm.Bold("antibuild build"), tm.BLUE) + ".\n" +
			"\n" +
			"To rebuild press " + tm.Color(tm.Bold("r"), tm.CYAN) + " and to hard reload press " + tm.Color(tm.Bold("R"), tm.CYAN) + ". Press " + tm.Color(tm.Bold("ESC"), tm.CYAN) + " to quit.")
	}

	printMessages(d.Log)
	d.Log.Messages = []logger.Message{}
	d.Log.MaxMessageType = logger.DEBUG

	tm.FlushOnlyVisible()
}

func getIP() string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "unknown"
	}

	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String()
			}
		}
	}

	return ""
}
