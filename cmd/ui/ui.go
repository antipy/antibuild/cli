// Copyright © 2020 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package ui contains all functions for displaying things to the user
package ui

import (
	tm "github.com/buger/goterm"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

func printMessages(log *logger.Logger) {
	if len(log.Messages) > 0 {
		for _, m := range log.Messages {
			tm.Println(FormatMessage(m, FormatErrorCMD))
		}
	}
}

func reset() {
	tm.Clear()
	tm.MoveCursor(0, 0)
}
