package ui

import (
	"fmt"

	tm "github.com/buger/goterm"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	//Loader is a struct for a loading printer
	Loader struct {
		Total   int
		current int
	}
)

// Update prints a loading bar, if you set the progress to
// -1 it will use the previously set progress
func (l *Loader) Update(progress int, log *logger.Logger) {
	if progress != -1 {
		l.current = progress
	}
	reset()
	if log != nil {
		printMessages(log)
	}
	prefix := fmt.Sprintf("%d / %d [", l.current, l.Total)
	tm.Print(prefix)

	width := tm.Width() - (len(prefix) + 1) - 5 //+1 for the last character -5 for spacing
	newCurrent := (width * l.current) / l.Total //now its scaled up tot he width of the bar
	for i := 0; i < width; i++ {
		switch {
		case i == newCurrent:
			tm.Print(">")
		case i > newCurrent:
			tm.Print("-")
		case i < newCurrent:
			tm.Print("=")
		}
	}

	tm.Println("]")

	tm.FlushOnlyVisible()
}
