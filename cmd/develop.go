// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/engine"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

func developCommandRun(command *cobra.Command, args []string) {
	port := getString(command, portFlag.Name, portFlag.Value)
	configfile := getString(command, configFlag.Name, configFlag.Value)
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	develop := ui.Develop{}
	develop.Port = port
	develop.Log = log

	cfg, err := localConfig.GetConfig(configfile)
	if err != nil {
		log.Fatale(err)
		return
	}

	engine.Start(cfg, true, port, log, &develop)
}
