// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/engine"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

func buildCommandRun(command *cobra.Command, args []string) {
	configfile := getString(command, configFlag.Name, configFlag.Value)
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	build := ui.Builder{}
	build.Log = log
	if os.Getenv("DEBUG") == "1" { //cant get out of this, itl just loop
		engine.StartDebugLoop(configfile, log)
		return
	}

	cfg, err := localConfig.GetConfig(configfile)
	if err != nil {
		log.Fatale(err)
		build.Update()
		return
	}

	engine.Start(cfg, false, "", log, &build)
	if build.Log.MaxMessageType == logger.FATAL {
		build.Update()
		log.Close()
		os.Exit(1)
	}
}
