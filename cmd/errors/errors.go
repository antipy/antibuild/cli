// Copyright © 2018-2020 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package errors contains all the errors for the CMD
package errors

//ErrFailedModuleRepositoryDownload means the module repository list download failed
type ErrFailedModuleRepositoryDownload struct {
	RepositoryURL string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedModuleRepositoryDownload) Error() string {
	return "module repository list download failed"
}

// --------------

//ErrNotExist means the module does not exist in the repository
type ErrNotExist struct {
	Identifier string
	Repository string
	Version    string
}

// Error method for standard go errors
func (e ErrNotExist) Error() string {
	return "module does not exist in the repository"
}

// --------------

//ErrNoLatestSpecified means the module binary download failed
type ErrNoLatestSpecified struct {
	Identifier string
	Repository string
}

// Error method for standard go errors
func (e ErrNoLatestSpecified) Error() string {
	return "the module repository did not specify a latest version"
}

// --------------

//ErrFailedModuleBinaryDownload means the module binary download failed
type ErrFailedModuleBinaryDownload struct {
	Identifier    string
	Repository    string
	Version       string
	BinaryURL     string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedModuleBinaryDownload) Error() string {
	return "failed downloading module binary from repository server"
}

// --------------

//ErrUnkownSourceRepositoryType means that source repository type was not recognized
type ErrUnkownSourceRepositoryType struct {
	Identifier string
	Repository string
	Version    string
	SourceType string
}

// Error method for standard go errors
func (e ErrUnkownSourceRepositoryType) Error() string {
	return "the source repository type is not recognized"
}

// --------------

// ErrFailedGitRepositoryDownload means that the git repository could not be cloned
type ErrFailedGitRepositoryDownload struct {
	Identifier    string
	Repository    string
	Version       string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedGitRepositoryDownload) Error() string {
	return "the source repository type is unknown"
}

// --------------

// ErrFailedModuleBuild means that the module could not be built
type ErrFailedModuleBuild struct {
	Identifier    string
	Repository    string
	Version       string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedModuleBuild) Error() string {
	return "failed to build the module from repository source"
}

// --------------

// ErrGlobalConfigFailedOpen means the global config file could not be opened
type ErrGlobalConfigFailedOpen struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrGlobalConfigFailedOpen) Error() string {
	return "failed opening the global config file"
}

// --------------

// ErrGlobalConfigFailedParse means the global config file could not be parsed
type ErrGlobalConfigFailedParse struct {
	DetailedError error
}

// Error method for standard go errors
func (e ErrGlobalConfigFailedParse) Error() string {
	return "failed parsing the global config file"
}

// --------------

// ErrGlobalConfigFailedWrite means the config file could not be written to
type ErrGlobalConfigFailedWrite struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrGlobalConfigFailedWrite) Error() string {
	return "failed writing to the global config file"
}

// --------------

// ErrRepositoryAlreadyInGlobalConfig means the repository is already in the global config
type ErrRepositoryAlreadyInGlobalConfig struct {
	Repository string
}

// Error method for standard go errors
func (e ErrRepositoryAlreadyInGlobalConfig) Error() string {
	return "repository is already in the global config"
}

// --------------

// ErrRepositoryNotInGlobalConfig means the repository is not found in the global config
type ErrRepositoryNotInGlobalConfig struct {
	Repository string
}

// Error method for standard go errors
func (e ErrRepositoryNotInGlobalConfig) Error() string {
	return "repository is not in the global config"
}

// --------------

// ErrInvalidInputType means that your input type is not correct
type ErrInvalidInputType struct {
	Field string
	Type  string
}

// Error method for standard go errors
func (e ErrInvalidInputType) Error() string {
	return "input type is not correct"
}

// --------------

// ErrInvalidName means the name you entered is not valid
type ErrInvalidName struct {
	Name string
}

// Error method for standard go errors
func (e ErrInvalidName) Error() string {
	return "the name is not valid"
}

// --------------

// ErrModuleNotInConfig means the module is not in the config
type ErrModuleNotInConfig struct {
	Identifier string
}

// Error method for standard go errors
func (e ErrModuleNotInConfig) Error() string {
	return "module is not in the config"
}

// --------------

//ErrFailedTemplateRepositoryDownload means the template repository list download failed
type ErrFailedTemplateRepositoryDownload struct {
	RepositoryURL string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedTemplateRepositoryDownload) Error() string {
	return "template repository list download failed"
}

// --------------

// ErrTemplateNotAvailableInRepository means the template is not available in the repo
type ErrTemplateNotAvailableInRepository struct {
	Identifier string
	Repository string
}

// Error method for standard go errors
func (e ErrTemplateNotAvailableInRepository) Error() string {
	return "template not available in repository"
}
