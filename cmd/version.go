// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"text/template"

	"github.com/spf13/cobra"
)

const version = "1.0.0-pre3"

func versionCommandRun(command *cobra.Command, arguments []string) {
	t := template.New("version")
	template.Must(t.Parse(antibuildCommand.VersionTemplate()))
	err := t.Execute(antibuildCommand.OutOrStdout(), antibuildCommand)
	if err != nil {
		antibuildCommand.Println(err)
	}
}
