// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"github.com/spf13/pflag"
	"gitlab.com/antipy/antibuild/cli/cmd/modules/repositories"
)

var (
	configFlag = pflag.Flag{
		Name:      "config",
		Shorthand: "c",
		Usage:     "Config file that should be used for building. If not specified will use config.json.",
		Value:     newStringValue("config.json"),
	}

	debugFlag = pflag.Flag{
		Name:      "debug",
		Shorthand: "d",
		Usage:     "If the debug log should pe printed to the log and terminal.",
		Value:     newBoolValue(false),
	}

	logfileFlag = pflag.Flag{
		Name:      "logfile",
		Shorthand: "l",
		Usage:     "The logfile to use for putting the logs.",
		Value:     newStringValue(""),
	}
	//TODO: should this be an interger? maybe cuz the port should be an interger.
	portFlag = pflag.Flag{
		Name:      "port",
		Shorthand: "p",
		Usage:     "The port that is used to host the development server.",
		Value:     newStringValue("8080"),
	}

	temlaterepoFlag = pflag.Flag{
		Name:      "templates",
		Shorthand: "t",
		Usage:     "The template repository list file to use. Default is \"https://build.antipy.com/dl/templates.json\"",
		Value:     newStringValue(defaultTemplateRepositoryURL),
	}

	templatebranchFlag = pflag.Flag{
		Name:      "branch",
		Shorthand: "b",
		Usage:     "The branch to use from the template repository if using git.",
		Value:     newStringValue(defaultTemplateBranch),
	}

	modulesrepoFlag = pflag.Flag{
		Name:      "modules",
		Shorthand: "m",
		Usage:     "The module repository to use.",
		Value:     newStringValue(repositories.NoRepositorySpecified),
	}
)
