// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/cmd/modules/repositories"
	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	globalConfig "gitlab.com/antipy/antibuild/cli/configuration/global"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

// repoAddCommandRun is the cobra command
func repoAddCommandRun(command *cobra.Command, args []string) {
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfgGlobal, err := globalConfig.Open()
	if err != nil {
		log.Fatale(errors.ErrGlobalConfigFailedOpen{
			Path:          "unknown",
			DetailedError: err,
		})
		ui.PrintGeneric(log)
		return
	}

	err = repositories.AddRepository(args[0], cfgGlobal)
	if err != nil {
		log.Warninge(err)
	}
	err = cfgGlobal.Save()
	if err != nil {
		log.Errore(errors.ErrGlobalConfigFailedWrite{
			Path:          "unknown",
			DetailedError: err,
		})
	}

	ui.PrintGeneric(log)
}

// repoListCommandRun is the cobra command
func repoListCommandRun(command *cobra.Command, args []string) {
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfgGlobal, err := globalConfig.Open()
	if err != nil {
		log.Fatale(errors.ErrGlobalConfigFailedOpen{
			Path:          "unknown",
			DetailedError: err,
		})
		ui.PrintGeneric(log)
		return
	}

	ui.PrintList(cfgGlobal.Repositories)
}

// repoRemoveCommandRun is the cobra command
func repoRemoveCommandRun(command *cobra.Command, args []string) {
	debugEnabled := getBool(command, debugFlag.Name, debugFlag.Value)
	logfile := getString(command, logfileFlag.Name, logfileFlag.Value)

	log := logger.New(debugEnabled, logfile)
	defer log.Close()

	cfgGlobal, err := globalConfig.Open()
	if err != nil {
		log.Fatale(errors.ErrGlobalConfigFailedOpen{
			Path:          "unknown",
			DetailedError: err,
		})
		ui.PrintGeneric(log)
		return
	}

	repositories.RemoveRepository(args[0], cfgGlobal, log)

	err = cfgGlobal.Save()
	if err != nil {
		log.Errore(errors.ErrGlobalConfigFailedWrite{
			Path:          "unknown",
			DetailedError: err,
		})
	}

	ui.PrintGeneric(log)
}
