// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package templates manages downloading and installing templates
package templates

import (
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.com/antipy/antibuild/cli/internal/errors"

	cmderrors "gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/internal"
	"gitlab.com/antipy/antibuild/cli/internal/download"
	"gitlab.com/antipy/antibuild/cli/internal/zip"
)

// Entry is a single entry for a template repository file
type Entry struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Source      struct {
		Type         string `json:"type"`
		URL          string `json:"url"`
		SubDirectory string `json:"subdirectory"`
	} `json:"source"`
}

// GetRepository from a url
func GetRepository(url string) (map[string]Entry, error) {
	repo := make(map[string]Entry)
	err := download.JSON(url, &repo)
	if err != nil {
		return nil, cmderrors.ErrFailedTemplateRepositoryDownload{
			RepositoryURL: url,
			DetailedError: err,
		}
	}

	return repo, nil
}

// Download a template
func Download(templateRepository map[string]Entry, template, outPath, templateBranch string) error {
	if _, ok := templateRepository[template]; !ok {
		return cmderrors.ErrTemplateNotAvailableInRepository{
			Identifier: template,
			Repository: "unknown",
		}
	}

	t := templateRepository[template]

	dir, err := ioutil.TempDir("", "antibuild")

	if err != nil {
		return errors.ErrFailedCreateFolder{
			Path:          dir,
			DetailedError: err,
		}
	}

	defer os.RemoveAll(dir)

	var src string

	switch t.Source.Type {
	case "zip":
		downloadFilePath := filepath.Join(dir, "download.zip")

		err = download.File(downloadFilePath, t.Source.URL, false)
		if err != nil {
			return err
		}

		err = zip.Unzip(downloadFilePath, dir)
		if err != nil {
			return err
		}
		os.Remove(downloadFilePath) //we dont want the zip hanging around in the template
		src = filepath.Join(dir, t.Source.SubDirectory)

	case "git":
		err = download.Git(dir, t.Source.URL, templateBranch)
		if err != nil {
			return err
		}

		err = os.RemoveAll(filepath.Join(dir, ".git"))
		if err != nil {
			return errors.ErrFailedRemoveFile{
				FileLocation:  dir,
				DetailedError: err,
			}
		}

		src = filepath.Join(dir, t.Source.SubDirectory)
	}

	info, err := os.Lstat(src)
	if err != nil {
		return err
	}

	err = internal.DirCopy(src, outPath, info)
	if err != nil {
		return err
	}

	return nil
}
