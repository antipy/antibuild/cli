// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package main

import (
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

/* 	copied and modified from github.com/spf13/pflag
licenced under 'BSD 3-Clause "New" or "Revised" License'
at june 6th 2019 */
type stringValue string

func newStringValue(val string) *stringValue {
	return (*stringValue)(&val)
}

func (s *stringValue) Set(val string) error {
	*s = stringValue(val)
	return nil
}
func (s *stringValue) Type() string {
	return "string"
}

func (s *stringValue) String() string {
	return string(*s)
}

func getString(cmd *cobra.Command, flag string, value pflag.Value) string {
	ret, err := cmd.Flags().GetString(flag)
	if err != nil { //empty flag isnt invalid, err is the only reliable way
		return value.String()
	}
	return ret
}

// -- bool Value
type boolValue bool

func newBoolValue(val bool) *boolValue {
	return (*boolValue)(&val)
}

func (b *boolValue) Set(s string) error {
	v, err := strconv.ParseBool(s)
	*b = boolValue(v)
	return err
}

func (b *boolValue) Type() string {
	return "bool"
}

func (b *boolValue) String() string {
	return strconv.FormatBool(bool(*b))
}

func (b *boolValue) IsBoolFlag() bool {
	return true
}

func getBool(cmd *cobra.Command, flag string, value pflag.Value) bool {
	ret, err := cmd.Flags().GetBool(flag)
	if err != nil { //empty flag isnt invalid, err is the only reliable way
		if v, ok := value.(*boolValue); ok {
			return bool(*v)
		}
		return false
	}
	return ret
}
