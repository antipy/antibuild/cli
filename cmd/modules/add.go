// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"gitlab.com/antipy/antibuild/cli/cmd/ui"
	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

//AddModule adds a module to the local config
func AddModule(cfg *localConfig.Config, module, repo string, log *logger.Logger) {
	newModule, err := modules.ParseModuleString(module)
	if err != nil {
		log.Fatale(err)
		return
	}

	//TODO: make a new function that doesnt need this BS.
	newModule.Name = newModule.Repository
	newModule.Repository = repo

	log.Infof("Installing %s", newModule.Repository)
	bar := ui.Loader{}
	bar.Total = 1
	bar.Update(0, log)
	installedModule, err := InstallModule(*newModule, cfg.Folders.Modules, log)
	if err != nil {
		log.Fatale(err)
		return
	}

	log.Infof("Finished installing %s at version %s from %s",
		newModule.Name, installedModule.Version, installedModule.Repository)
	bar.Update(1, log)
	installedModule.Name = newModule.Name
	cfg.Modules.Dependencies = append(cfg.Modules.Dependencies, installedModule)
}
