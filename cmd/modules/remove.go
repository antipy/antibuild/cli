// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"os"

	cmderrors "gitlab.com/antipy/antibuild/cli/cmd/errors"
	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

//RemoveModule removes a module from the config
func RemoveModule(cfg *localConfig.Config, module string, log *logger.Logger) error {
	for k, v := range cfg.Modules.Dependencies {
		if v.Name == module { //if the module has been found. remove it

			// Replace it with the last one. CAREFUL only works if you have enough elements.
			cfg.Modules.Dependencies[k] = cfg.Modules.Dependencies[len(cfg.Modules.Dependencies)-1]
			cfg.Modules.Dependencies = cfg.Modules.Dependencies[:len(cfg.Modules.Dependencies)-1] // Chop off the last one.

			location := cfg.Folders.Modules + "/abm_" + module + "@" + v.Version.String()

			err := os.Remove(location)
			if err != nil {
				return cmderrors.ErrModuleNotInConfig{
					Identifier: module,
				}
			}
			return nil
		}
	}

	return cmderrors.ErrModuleNotInConfig{
		Identifier: module,
	}
}
