// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package modules contains all the functions for installing modules
package modules

import (
	"io/ioutil"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/blang/semver"
	tm "github.com/buger/goterm"
	cmderrors "gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/cmd/modules/repositories"
	globalConfig "gitlab.com/antipy/antibuild/cli/configuration/global"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal"
	"gitlab.com/antipy/antibuild/cli/internal/compile"
	"gitlab.com/antipy/antibuild/cli/internal/download"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

//InstallModule installs a module
func InstallModule(meta modules.Module, filePrefix string, log *logger.Logger) (modules.Module, error) {
	var repoURLs []string

	if meta.Repository == repositories.NoRepositorySpecified {
		cfgGlobal, err := globalConfig.Open()
		if err != nil {
			return modules.Module{}, cmderrors.ErrGlobalConfigFailedOpen{
				Path:          "unknown",
				DetailedError: err,
			}
		}

		repoURLs = []string{
			repositories.StandardRepository,
		}
		repoURLs = append(repoURLs, cfgGlobal.Repositories...)
	} else {
		repoURLs = []string{
			meta.Repository,
		}
	}

	for _, rURL := range repoURLs {
		repo := &repositories.Repository{}

		err := repo.Download(rURL)
		if err != nil {
			return modules.Module{}, err
		}

		if me, ok := (*repo)[meta.Name]; ok {
			if meta.Version.Equals(modules.LatestVersion) {
				if me.LatestVersion.Equals(semver.Version{}) {
					return modules.Module{}, cmderrors.ErrNoLatestSpecified{
						Identifier: meta.Name,
						Repository: rURL,
					}
				}

				meta.Version = me.LatestVersion
			}
			moduleWithRepo := modules.Module{
				Name:       meta.Name,
				Version:    meta.Version,
				Repository: rURL,
			}
			if modules.InternalModules.Contains(moduleWithRepo) == modules.HaveSameVersion {
				log.Info("Module is available internally. There is no need to download.")
				return moduleWithRepo, nil
			}

			installedVersion, err := Install(me, meta.Version, filepath.Join(filePrefix, "abm_"+meta.Name))
			if err != nil {
				if _, ok := err.(cmderrors.ErrNotExist); ok {
					continue
				}

				return modules.Module{}, err
			}

			return modules.Module{
				Repository: rURL,
				Version:    installedVersion,
				Name:       meta.Name,
			}, nil
		}
	}

	return modules.Module{}, cmderrors.ErrNotExist{
		Identifier: meta.Name,
		Version:    meta.Version.String(),
		Repository: meta.Repository,
	}
}

//Install installs a module from a module entry
func Install(me repositories.Entry, version semver.Version, targetFile string) (semver.Version, error) {
	versionString := version.String()
	targetFile += "@" + versionString

	goos := runtime.GOOS
	goarch := runtime.GOARCH

	// static compiled modules
	if ver, ok := me.CompiledStatic[versionString]; ok {
		if os, ok := ver[goos]; ok {
			if downloadURL, ok := os[goarch]; ok {
				err := download.File(targetFile, downloadURL, true)
				if err != nil {
					return semver.Version{}, cmderrors.ErrFailedModuleBinaryDownload{
						Identifier:    targetFile,
						Version:       versionString,
						Repository:    "unknown",
						BinaryURL:     downloadURL,
						DetailedError: err,
					}
				}

				return version, nil
			}
		}
	}
	compiledDynamic := me.CompiledDynamic
	// dynamic compiled modules
	if me.CompiledDynamic.URL != "" && internal.ContainsVerson(me.CompiledDynamic.Vesions, version) {
		if _, ok := compiledDynamic.OSArchCombos[goos]; ok && internal.Contains(compiledDynamic.OSArchCombos[goos], goarch) {
			url := strings.ReplaceAll(me.CompiledDynamic.URL, "{{version}}", versionString)
			url = strings.ReplaceAll(url, "{{os}}", goos)
			url = strings.ReplaceAll(url, "{{arch}}", goarch)

			tm.Print(tm.Color("Using "+tm.Bold(url), tm.BLUE) + tm.Color(" for download.", tm.BLUE) + "\n")
			tm.FlushAll()
			err := download.File(targetFile, url, true)
			if err != nil {
				return semver.Version{}, cmderrors.ErrFailedModuleBinaryDownload{
					Identifier:    targetFile,
					Version:       versionString,
					Repository:    "unknown",
					BinaryURL:     url,
					DetailedError: err,
				}
			}

			return version, nil
		}
	}

	// local compiled modules
	dir, err := ioutil.TempDir("", "abm_"+me.Name+"@"+versionString)
	if err != nil {
		return semver.Version{}, errors.ErrFailedOpenFile{
			FileLocation:  "unknow",
			DetailedError: err,
		}
	}

	switch me.Source.Type {
	case "git":
		v := versionString
		if me.Source.SubDirectory != "" {
			v = me.Source.SubDirectory + "/" + versionString
		}

		err = download.Git(dir, me.Source.URL, v)
		if err != nil {
			return semver.Version{}, cmderrors.ErrFailedGitRepositoryDownload{
				Identifier:    targetFile,
				Version:       versionString,
				Repository:    "unknown",
				DetailedError: err,
			}
		}

		dir = filepath.Join(dir, filepath.Base(me.Source.URL))
	default:
		return semver.Version{}, cmderrors.ErrUnkownSourceRepositoryType{
			Identifier: targetFile,
			Version:    versionString,
			Repository: "unknown",
			SourceType: me.Source.Type,
		}
	}

	dir = filepath.Join(dir, me.Source.SubDirectory)
	err = compile.FromSource(dir, targetFile)
	if err != nil {
		return semver.Version{}, cmderrors.ErrFailedModuleBuild{
			Identifier:    targetFile,
			Version:       versionString,
			Repository:    "unknown",
			DetailedError: err,
		}
	}

	return version, nil
}
