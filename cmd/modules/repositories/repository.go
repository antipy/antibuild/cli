// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package repositories manages all the repositories for downloading modules
package repositories

import (
	"github.com/blang/semver"
	"gitlab.com/antipy/antibuild/cli/cmd/errors"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/download"
)

type (
	// Entry is a single entry for a module repository file
	Entry struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		Source      struct {
			Type         string `json:"type"`
			URL          string `json:"url"`
			SubDirectory string `json:"subdirectory"`
		} `json:"source"`
		CompiledStatic  map[string]map[string]map[string]string `json:"compiled_static"`
		CompiledDynamic struct {
			URL          string              `json:"url"`
			Vesions      []semver.Version    `json:"versions"`
			OSArchCombos map[string][]string `json:"os_arch_combos"`
		} `json:"compiled_dynamic"`
		LatestVersion semver.Version `json:"latest"`
	}

	// Repository is a repository for modules
	Repository map[string]Entry
)

const (
	// StandardRepository for modules
	StandardRepository = modules.StandardRepository

	// NoRepositorySpecified is when you dont pass the -m flag
	NoRepositorySpecified = ""
)

//Download downloads a json file into a module repository
func (m *Repository) Download(url string) error {
	err := download.JSON(url, m)
	if err != nil {
		return errors.ErrFailedModuleRepositoryDownload{
			RepositoryURL: url,
			DetailedError: err,
		}
	}

	return nil
}
