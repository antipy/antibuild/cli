// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package repositories

import (
	"gitlab.com/antipy/antibuild/cli/cmd/errors"
	globalConfig "gitlab.com/antipy/antibuild/cli/configuration/global"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

func RemoveRepository(repo string, cfgGlobal *globalConfig.Config, log *logger.Logger) {
	for i, crepo := range cfgGlobal.Repositories {
		if crepo == repo {
			cfgGlobal.Repositories[i] = cfgGlobal.Repositories[len(cfgGlobal.Repositories)-1]
			cfgGlobal.Repositories = cfgGlobal.Repositories[:len(cfgGlobal.Repositories)-1]
			err := cfgGlobal.Save()
			if err != nil {
				log.Fatale(errors.ErrGlobalConfigFailedWrite{
					Path:          "unknown",
					DetailedError: err,
				})
				return
			}

			return
		}
	}
	log.Warninge(errors.ErrRepositoryNotInGlobalConfig{
		Repository: repo,
	})
}
