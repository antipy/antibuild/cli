// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package repositories

import (
	"gitlab.com/antipy/antibuild/cli/cmd/errors"
	globalConfig "gitlab.com/antipy/antibuild/cli/configuration/global"
)

//AddRepository adds a repository to a global config
func AddRepository(repo string, globalcfc *globalConfig.Config) error {
	for _, crepo := range globalcfc.Repositories {
		if crepo == repo {
			return errors.ErrRepositoryAlreadyInGlobalConfig{
				Repository: repo,
			}
		}
	}

	globalcfc.Repositories = append(globalcfc.Repositories, repo)
	return nil
}
