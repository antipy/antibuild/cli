// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package local contains the types for a local configuration
package local

import (
	"encoding/json"
	"os"
	"path"

	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/engine/site"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (
	//Config is the config struct
	Config struct {
		Folders    Folder           `json:"folders"`
		Modules    modules.Modules  `json:"modules"`
		Pages      *site.ConfigSite `json:"pages"`
		configPath string
	}

	//Folder is the part of the config file that handles folders
	Folder struct {
		Templates string `json:"templates"`
		Static    string `json:"static"`
		Output    string `json:"output"`
		Modules   string `json:"modules"`
	}
)

//RefreshConfig will reload the config from disk.
func (cfg *Config) RefreshConfig() error {
	ncfg, err := GetConfig(cfg.configPath)
	if err != nil {
		return err
	}
	*cfg = *ncfg
	return nil
}

//GetTemplateFolder will returns the absolute path of the template folder
func (cfg Config) GetTemplateFolder() string {
	return cfg.Folders.Templates
}

//GetStaticFolder will returns the absolute path of the static folder
func (cfg Config) GetStaticFolder() string {
	return cfg.Folders.Static
}

//GetOutputFolder will returns the absolute path of the output folder
func (cfg Config) GetOutputFolder() string {
	return cfg.Folders.Output
}

//GetModulesFolder will returns the absolute path of the modules folder
func (cfg Config) GetModulesFolder() string {
	return cfg.Folders.Modules
}

//GetRootSite will return the root site.
func (cfg *Config) GetRootSite() site.ConfigSite {
	return *cfg.Pages
}

//GetModuleConfig will returns the absolute path of the modules folder
func (cfg Config) GetModuleConfig() modules.Modules {
	return cfg.Modules
}

//GetConfigLoc will returns the absolute path of the modules folder
func (cfg Config) GetConfigLoc() string {
	return cfg.configPath
}

//Save will save the config to disk.
func (cfg *Config) Save() error {
	file, err := os.Create(cfg.configPath + ".new")
	if err != nil {
		return errors.ErrConfigFailedOpen{
			Path:          cfg.configPath,
			DetailedError: err,
		}
	}
	defer file.Close()

	encoder := json.NewEncoder(file)
	encoder.SetIndent("", "	")
	err = encoder.Encode(cfg)
	if err != nil {
		os.Remove(cfg.configPath + ".new")
		return errors.ErrConfigFailedWrite{
			Path:          cfg.configPath,
			DetailedError: err,
		}
	}

	err = os.Rename(cfg.configPath+".new", cfg.configPath)
	if err != nil {
		return errors.ErrConfigFailedWrite{
			Path:          cfg.configPath,
			DetailedError: err,
		}
	}

	return nil
}

//GetConfig gets the config file. DOES NOT CHECK FOR MISSING INFORMATION!!
func GetConfig(configLocation string) (*Config, error) {
	cfg, err := GetDirtyConfig(configLocation)
	if err != nil {
		return nil, err
	}

	if cfg.Folders.Templates == "" {
		return cfg, errors.ErrNoTemplateFolderSpecified{}
	}

	if cfg.Folders.Output == "" {
		return cfg, errors.ErrNoOutputFolderSpecified{}
	}

	return cfg, nil
}

//GetDirtyConfig gets the config file but doesnt check if any values are set.
//So for example it will be possible for the template path to not be available
func GetDirtyConfig(configLocation string) (*Config, error) {
	file, err := os.Open(configLocation)
	if err != nil {
		return nil, errors.ErrConfigFailedOpen{
			Path:          configLocation,
			DetailedError: err,
		}
	}
	defer file.Close()

	cfg := new(Config)

	_, err = file.Seek(0, 0)
	if err != nil {
		return nil, errors.ErrConfigFailedOpen{
			Path:          configLocation,
			DetailedError: err,
		}
	}

	err = json.NewDecoder(file).Decode(&cfg)
	if err != nil {
		return nil, errors.ErrConfigFailedParse{
			DetailedError: err,
		}
	}
	cfg.configPath = configLocation

	//cfg.Pages.Slug.Prepend(cfg.Folders.Output)
	// setTemplatePath(cfg.Pages, cfg.Folders.Templates)

	return cfg, nil
}

func setTemplatePath(site *site.ConfigSite, templatepath string) {
	for k := range site.Templates {
		site.Templates[k] = path.Join(templatepath, site.Templates[k])
	}
	for k := range site.Sites {
		setTemplatePath(&site.Sites[k], templatepath)
	}
}
