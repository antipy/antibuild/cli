// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package global

import (
	"encoding/json"
	"os"
	"path/filepath"

	"github.com/kirsle/configdir"
)

// Open opens and parses the global config file
func Open() (*Config, error) {
	configPath := getDefaultConfigPath()
	err := configdir.MakePath(filepath.Dir(configPath))
	if err != nil {
		return nil, err
	}

	c := &Config{}

	err = c.Load(configPath)
	if err != nil {
		return nil, err
	}

	return c, nil
}

// Load loads the global config
func (c *Config) Load(path string) error {
	newc := Config{}
	f, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			err = newc.Save()
			if err != nil {
				return err
			}
			*c = newc
			return nil
		}
		return err
	}

	defer f.Close()

	err = json.NewDecoder(f).Decode(&newc)
	if err != nil {
		return err
	}

	*c = newc
	return nil

}
