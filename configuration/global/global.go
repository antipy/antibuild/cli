// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package global contains the types for a global configuration
package global

import (
	"path/filepath"

	"github.com/kirsle/configdir"
)

type (
	// Config is a global antibuild configuration
	Config struct {
		Repositories []string `json:"repositories"`
	}
)

func getDefaultConfigPath() string {
	return filepath.Join(configdir.LocalConfig("antibuild"), "config.json")
}
