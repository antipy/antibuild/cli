// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package global

import (
	"encoding/json"
	"os"
)

// Save saves the global config
func (c *Config) Save() error {
	configPath := getDefaultConfigPath()
	f, err := os.Create(configPath)
	if err != nil {
		return err
	}

	err = json.NewEncoder(f).Encode(c)
	if err != nil {
		return err
	}

	return nil
}
