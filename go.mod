module gitlab.com/antipy/antibuild/cli

go 1.13

replace github.com/buger/goterm => github.com/lucacasonato/goterm v0.0.0-20200402151817-56622469eb89

require (
	github.com/AlecAivazis/survey/v2 v2.0.8
	github.com/Netflix/go-expect v0.0.0-20200312175327-da48e75238e2 // indirect
	github.com/blang/semver v3.5.1+incompatible
	github.com/buger/goterm v0.0.0-20200322175922-2f3e71b85129
	github.com/eiannone/keyboard v0.0.0-20200508000154-caf4b762e807
	github.com/fsnotify/fsnotify v1.4.9
	github.com/hinshun/vt10x v0.0.0-20180809195222-d55458df857c // indirect
	github.com/jaicewizard/tt v0.8.1
	github.com/kirsle/configdir v0.0.0-20170128060238-e45d2f54772f
	github.com/kr/pty v1.1.8 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.6.1 // indirect
	gitlab.com/antipy/antibuild/api v1.0.0-pre15
	gitlab.com/antipy/antibuild/std/deno v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/file v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/json v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/language v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/markdown v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/math v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/util v1.0.0-pre17
	gitlab.com/antipy/antibuild/std/yaml v1.0.0-pre17
	gitlab.com/opennota/wd v0.0.0-20191124020556-236695b0ea63 // indirect
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.3 // indirect
)
