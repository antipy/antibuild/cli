package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"
)

type TestConfiguration struct {
	Folder         string
	RefrenceFolder string
	ConfigFile     string
}

var buildTests = map[string]TestConfiguration{
	"simple-template": {
		Folder:     "simple-template",
		ConfigFile: "config.json",
	},
	"nested-template": {
		Folder:     "nested-template",
		ConfigFile: "config.json",
	},
	"multi-data": {
		Folder:     "multi-data",
		ConfigFile: "config.json",
	},
	"iterator-slug": {
		Folder:         "iterator",
		ConfigFile:     "slug.json",
		RefrenceFolder: "slug",
	},
	"iterator-nested-slug": {
		Folder:         "iterator",
		ConfigFile:     "nested-slug.json",
		RefrenceFolder: "nested-slug",
	},
	"iterator-range": {
		Folder:         "iterator",
		ConfigFile:     "range.json",
		RefrenceFolder: "range",
	},
	"iterator-nested-range": {
		Folder:         "iterator",
		ConfigFile:     "nested-range.json",
		RefrenceFolder: "nested-range",
	},
	"iterator-all": {
		Folder:         "iterator",
		ConfigFile:     "all.json",
		RefrenceFolder: "all",
	},
	"module-deno": {
		Folder:         "modules",
		ConfigFile:     "deno.json",
		RefrenceFolder: "deno",
	},
	"module-file": {
		Folder:         "modules",
		ConfigFile:     "file.json",
		RefrenceFolder: "file",
	},
	"module-json": {
		Folder:         "modules",
		ConfigFile:     "json.json",
		RefrenceFolder: "json",
	},
	"module-yaml": {
		Folder:         "modules",
		ConfigFile:     "yaml.json",
		RefrenceFolder: "yaml",
	},
	"module-util": {
		Folder:         "modules",
		ConfigFile:     "util.json",
		RefrenceFolder: "util",
	},
	"module-math": {
		Folder:         "modules",
		ConfigFile:     "math.json",
		RefrenceFolder: "math",
	},
	"module-markdown": {
		Folder:         "modules",
		ConfigFile:     "markdown.json",
		RefrenceFolder: "markdown",
	},
	"module-language": {
		Folder:         "modules",
		ConfigFile:     "language.json",
		RefrenceFolder: "language",
	},
}

func TestBuild(t *testing.T) {
	clean()

	buildBinary(t)

	if !t.Failed() {
		for name, config := range buildTests {
			t.Run(name, buildTest(config))
		}
	}

	if !t.Failed() {
		clean()
	}
}

func openLog(t *testing.T, name string) *os.File {
	file, err := os.Create(name + ".log")
	if err != nil {
		t.Fatal(err)
	}

	return file
}

// build the binary from source
func buildBinary(t *testing.T) {
	buildCommand := exec.Command("go", "build", "-o", "antibuild", "../cmd")
	log := openLog(t, "build_binary")
	defer log.Close()
	buildCommand.Stdout = log
	buildCommand.Stderr = log

	err := buildCommand.Run()
	if err != nil {
		t.Fatal(err.Error())
	}
}

// test if building with antibuild works and results in the right response
func buildTest(cfg TestConfiguration) func(t *testing.T) {
	return func(t *testing.T) {
		// build with antibuild
		log := openLog(t, filepath.Join(cfg.Folder, "build_native"))
		defer log.Close()

		err := build(t, cfg.Folder, cfg.ConfigFile, log)
		if err != nil {
			t.Error(err.Error())
			f, err := ioutil.ReadFile(filepath.Join(cfg.Folder, "build.log"))
			if err == nil {
				t.Log(string(f))
			}
			t.FailNow()
		}

		//compare the files
		err = compare(cfg.Folder, cfg.RefrenceFolder)
		if err != nil {
			t.Fatal(err.Error())
		}

	}
}

// build with antibuild
func build(t *testing.T, folder string, configFile string, log *os.File) error {
	buildCommand := exec.Command("../antibuild", "build", "--config", configFile, "--logfile", "build.log", "--debug", "true")

	t.Logf("building with command: '%s'", buildCommand)

	buildCommand.Stdout = log
	buildCommand.Stderr = log

	path, err := filepath.Abs(folder)
	if err != nil {
		return err
	}
	buildCommand.Dir = path

	err = buildCommand.Run()
	if err != nil {
		return err
	}

	return nil
}

// compare the folders
func compare(folder string, refrenceFolder string) error {
	refrenceDirectory, err := filepath.Abs(filepath.Join(folder, "refrence"))
	if err != nil {
		return err
	}

	if refrenceFolder != "" {
		refrenceDirectory = filepath.Join(refrenceDirectory, refrenceFolder)
	}

	publicDirectory, err := filepath.Abs(filepath.Join(folder, "public"))
	if err != nil {
		return err
	}

	err = filepath.Walk(refrenceDirectory, func(refrencePath string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}
		cleanPath := strings.TrimPrefix(refrencePath, refrenceDirectory)
		publicPath := filepath.Join(publicDirectory, cleanPath)

		refrenceFile, err := os.Open(refrencePath)
		if err != nil {
			return err
		}
		defer refrenceFile.Close()

		publicFile, err := os.Open(publicPath)
		if err != nil {
			return err
		}
		defer publicFile.Close()

		if !diffFile(refrenceFile, publicFile) {
			gitDiff(folder, cleanPath, refrencePath, publicPath)
			return fmt.Errorf("file %s does not match refrence", cleanPath)
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}

func diffFile(fileOne, fileTwo io.Reader) bool {
	chunkSize := 64
	for {
		b1 := make([]byte, chunkSize)
		_, err1 := fileOne.Read(b1)

		b2 := make([]byte, chunkSize)
		_, err2 := fileTwo.Read(b2)

		if err1 != nil || err2 != nil {
			if err1 == io.EOF && err2 == io.EOF {
				return bytes.Equal(b1, b2)
			} else if err1 == io.EOF || err2 == io.EOF {
				return false
			} else {
				return false
			}
		}

		if !bytes.Equal(b1, b2) {
			return false
		}
	}
}

func gitDiff(folder string, cleanPath string, refrencePath string, publicPath string) {
	diffCommand := exec.Command("git", "diff", "--no-index", "--minimal", refrencePath, publicPath)

	log, err := os.Create(filepath.Join(folder, strings.ReplaceAll(cleanPath, "/", "__")+"_diff.log"))
	if err != nil {
		return
	}
	defer log.Close()
	diffCommand.Stdout = log
	diffCommand.Stderr = log

	path, err := filepath.Abs(folder)
	if err != nil {
		return
	}
	diffCommand.Dir = path

	err = diffCommand.Run()
	if err != nil {
		return
	}
}

// build the binary from source
func clean() {
	binaryPath := []string{"antibuild"}
	publics, _ := filepath.Glob("*/public")
	logs, _ := filepath.Glob("*.log")
	logsNested, _ := filepath.Glob("**/*.log")

	for _, v := range append(logs, append(logsNested, append(publics, binaryPath...)...)...) {
		os.RemoveAll(v)
	}
}

// install all the modules required for a certain run
func installModules(cfg *TestConfiguration) func(t *testing.T) {
	return func(t *testing.T) {
		buildCommand := exec.Command("../antibuild", "modules", "install", "--config", cfg.ConfigFile, "--logfile", "module_install.log", "--debug", "true")
		log := openLog(t, filepath.Join(cfg.Folder, "module_install_native"))
		defer log.Close()
		buildCommand.Stdout = log
		buildCommand.Stderr = log

		path, err := filepath.Abs(cfg.Folder)
		if err != nil {
			t.Fatal(err.Error())
		}
		buildCommand.Dir = path

		err = buildCommand.Run()
		if err != nil {
			t.Fatal(err.Error())
		}
	}
}
