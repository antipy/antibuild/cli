// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package engine is the root of the actual system. The engine package contains
// the entry point to building using the given configuration.
package engine

import (
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/engine/site"
)

type (
	//Resulter is used for printing the result of building
	Resulter interface {
		Update()
	}

	//Configuration is an interface that provides all the configuration for antibuild.
	//All copies of the implementation must all point to the same struct. Thus the pointer
	//may not be modified.
	Configuration interface {
		//RefreshConfig will make sure that on the next use of the config the returned data will be up-to-dates unless error is not nil.
		//If err is not nil then the config must not have been updated.
		RefreshConfig() error

		//GetTemplateFolder will returns the absolute path of the template folder
		GetTemplateFolder() string

		//GetStaticFolder will returns the absolute path of the static folder
		GetStaticFolder() string

		//GetOutputFolder will returns the absolute path of the output folder
		GetOutputFolder() string

		//GetModulesFolder will returns the absolute path of the modules folder
		GetModulesFolder() string

		//GetRootSite will return the root site.
		GetRootSite() site.ConfigSite

		//GetModuleConfig will return the modules config.
		GetModuleConfig() modules.Modules
	}

	//BuildState is the buildstate for the engine
	BuildState struct {
		Configuration
		cache          cache
		moduleContext  *modules.ModuleContext
		configLocation string //this is only needed for the development mode
		OPPLocations   []string
		refreshEnabled bool
	}
)
