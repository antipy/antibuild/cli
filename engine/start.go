// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package engine

import (
	"os"
	"time"

	localConfig "gitlab.com/antipy/antibuild/cli/configuration/local"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
	"gitlab.com/antipy/antibuild/cli/internal/net"
)

//Start the build process
func Start(cfg Configuration, isRefreshEnabled bool, port string, log *logger.Logger, result Resulter) {

	if port != "" {
		go func() {
			err := net.HostLocally(cfg.GetOutputFolder(), port) // runs in background, hosting doesn't actually build
			log.Fatale(err)
			result.Update()
			os.Exit(1)
		}()
	}
	build := &BuildState{
		refreshEnabled: isRefreshEnabled,
		Configuration:  cfg,
	}

	if isRefreshEnabled { // if refresh is enabled run the refresh, if it returns return
		if v, ok := cfg.(DevelopConfiguration); ok {
			build.configLocation = v.GetConfigLoc()
		}
		buildOnRefresh(build, result, log)
	} else {
		err := startParse(build, log)
		build.Cleanup()
		result.Update()
		if err != nil {
			log.Fatale(err)
			//dont update as the last thing to do.
			//it wont show anything as it will be updated afterwards as well
			return
		}
	}
}

// StartDebugLoop loops builds a project for one minute and then prints how often it build
func StartDebugLoop(configLocation string, log *logger.Logger) {
	cfg, err := localConfig.GetConfig(configLocation)
	if err != nil {
		panic(err)
	}
	build := &BuildState{
		Configuration: cfg,
	}

	startParse(build, log)
	go func() {
		err := net.HostDebug()
		if err != nil {
			log.Errore(err)
		}
	}()
	timeout := time.After(1 * time.Minute)

	for i := 0; ; i++ {
		select {
		case <-timeout:
			println("did", i, "iterations int one minute")
			build.Cleanup()
			return
		default:
			build.cache.configUpdate = true
			build.cache.rootPage = cfg.GetRootSite()
			err = startCachedParse(build, log)
			if err != nil {
				//this is for benchmarking, if something is wrong then the benchmark is invalid
				panic(err)
			}
		}
	}
}

func startParse(build *BuildState, log *logger.Logger) error {
	if build.cache.data == nil { //if there isnt any cache already
		build.cache = cache{
			rootPage:     build.GetRootSite(),
			data:         make(map[string]cacheData),
			configUpdate: true,
			checkData:    false,
		}
	}

	return startCachedParse(build, log)
}
