// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package engine

import (
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	"github.com/eiannone/keyboard"

	"strings"

	"github.com/fsnotify/fsnotify"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	//DevelopConfiguration adds the getConfigLoc to the configuration. this is needed for use in waatching the config file
	DevelopConfiguration interface {
		Configuration
		//GetConfigLoc retuurns the configfile location. If getConfigLoc returns an empty string
		//if antibuild doesnt need to watch a config file
		GetConfigLoc() string
	}
)

//watches files and folders and rebuilds when things change
func buildOnRefresh(build *BuildState, result Resulter, log *logger.Logger) {
	defer build.Cleanup()

	build.moduleContext = &modules.ModuleContext{}
	build.moduleContext.InitLog()

	err := startParse(build, log)
	if err != nil {
		log.Fatale(err)
		result.Update()
		return
	}
	result.Update()
	dataloaders := build.moduleContext.StopLog()

	err = watchBuild(build, result, dataloaders, log)
	if err != nil {
		log.Fatale(err)
		result.Update()
		return
	}
}

//! modules will not be able to call a refresh and thus we can only use the (local) templates and static files as a source
func watchBuild(build *BuildState, result Resulter, dataloaders map[modules.ModuleFunctionID][]string, log *logger.Logger) error {

	refreshDataWatchers(build, result, dataloaders, log)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return errors.ErrFailedToStartFileWatcher{
			Identifier:    "template/config",
			DetailedError: err,
		}
	}

	//add template folder to watcher
	err = filepath.Walk(build.GetTemplateFolder(), func(path string, file os.FileInfo, err error) error {
		return watcher.Add(path)
	})
	if err != nil {
		return errors.ErrFailedToStartFileWatcher{
			Identifier:    "template/config",
			DetailedError: err,
		}
	}

	if build.GetStaticFolder() != "" {
		//add static folder to watcher
		err = filepath.Walk(build.GetStaticFolder(), func(path string, file os.FileInfo, err error) error {
			return watcher.Add(path)
		})

		if err != nil {
			return errors.ErrFailedToStartFileWatcher{
				Identifier:    "static/config",
				DetailedError: err,
			}
		}
	}

	err = watcher.Add(build.configLocation)
	if err != nil {
		return errors.ErrFailedToStartFileWatcher{
			Identifier:    "template/config",
			DetailedError: err,
		}
	}

	err = keyboard.Open()
	if err != nil {
		return errors.ErrFailedToStartKeyboardListener{
			DetailedError: err,
		}
	}
	defer keyboard.Close()

	keyChannel := make(chan rune)
	shutdown := make(chan int)
	go func() {
		for {
			char, key, _ := keyboard.GetKey()
			switch {
			case key == keyboard.KeyCtrlC || key == keyboard.KeyEsc || char == 'q':
				shutdown <- 1
			default:
				keyChannel <- char
			}
		}
	}()
	go func() {
		gracefulStop := make(chan os.Signal, 100)
		signal.Notify(gracefulStop, syscall.SIGINT, syscall.SIGTERM)
		<-gracefulStop
		shutdown <- 1
	}()

	for {
		//listen for watcher events
		select {
		case e, ok := <-watcher.Events:
			if !ok {
				return errors.ErrFileWatcherCrashed{
					Identifier:    "template/config",
					DetailedError: err,
				}
			}

			if e.Op != fsnotify.Create && e.Op != fsnotify.Remove && e.Op != fsnotify.Rename && e.Op != fsnotify.Write {
				break
			}

			log.Debugf("Refreshing because %s", e.Op)
			templateRoot, _ := filepath.Abs(build.GetTemplateFolder())
			file, _ := filepath.Abs(e.Name)

			switch {
			case e.Name == build.configLocation:
				log.Info("Changed file is config. Reloading...")
				err := build.RefreshConfig()
				if err != nil {
					log.Fatale(err)
					result.Update()
					continue
				} else {
					build.cache.configUpdate = true
				}
				build.moduleContext.InitLog()
			case strings.HasPrefix(file, templateRoot):
				build.cache.templateUpdate = file
			default:
				log.Infof("Refreshing because of page %s", e.Name)
			}
			err = startCachedParse(build, log)
			if err != nil {
				log.Fatale(err)
				result.Update()
				continue
			}

			result.Update()
			if e.Name == build.configLocation {
				newloaders := build.moduleContext.StopLog()
				refreshDataWatchers(build, result, newloaders, log)
			}

		case key := <-keyChannel:
			err := handleKey(key, build, log)
			if err != nil {
				return err
			}

			result.Update()
		case err, ok := <-watcher.Errors:
			if !ok {
				return errors.ErrFileWatcherCrashed{
					Identifier:    "template/config",
					DetailedError: err,
				}
			}
		case <-shutdown:
			return nil
		}
	}
}

func refreshDataWatchers(build *BuildState, result Resulter, dataloaders map[modules.ModuleFunctionID][]string, log *logger.Logger) {
	for id, data := range dataloaders {
		id := id
		log.Debug("Trying to watch dataloader: " + id.String())
		dataloader := build.moduleContext.GetDataWatcher(id)
		if dataloader != nil {
			dataloader.WatchCallback(data, func(i interface{}, e error) bool {
				file := i.(*string)
				log.Debug("Received update for file: " + *file + " and module: " + id.String())
				if *file == "KILL" {
					return false
				}
				split := strings.SplitAfterN(*file, ":", 2)
				if len(split) != 2 {
					log.Warning("Could not read which file changed from module: " + id.String())
				}

				build.cache.dataLoaderUpdate.id = id
				build.cache.dataLoaderUpdate.variable = split[1]

				err := startCachedParse(build, log)
				if err != nil {
					log.Fatale(err)
				}
				result.Update()

				return true
			})
		} else {
			log.Error("ISNIL")
		}
	}

}

func handleKey(key rune, build *BuildState, log *logger.Logger) error {
	switch key {
	case 'R':
		log.Info("Reloading config...")
		err := build.RefreshConfig()
		if err != nil {
			return err
		}

		build.cache.configUpdate = true
		build.cache.checkData = true

		err = startCachedParse(build, log)
		if err != nil {
			return err
		}
	case 'r':
		log.Info("Refreshing pages...")
		build.cache.checkData = true
		err := startCachedParse(build, log)
		if err != nil {
			return err
		}
	}

	return nil
}
