// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package engine

import (
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"time"

	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/engine/site"
	"gitlab.com/antipy/antibuild/cli/internal"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	cache struct {
		rootPage site.ConfigSite
		data     map[string]cacheData

		configUpdate     bool
		checkData        bool
		templateUpdate   string //the absolute path to the updated template
		dataLoaderUpdate struct {
			id       modules.ModuleFunctionID
			variable string
		}
	}

	cacheData struct {
		dependencies []string
		site         site.Site
		shouldRemove bool
		outputs      []string
	}
)

//Cleanup should cleanup
func (b *BuildState) Cleanup() {
	b.moduleContext.RemoveAll()
}

func startCachedParse(build *BuildState, log *logger.Logger) error {
	start := time.Now()

	if build.GetOutputFolder() == "" {
		return errors.ErrNoOutputFolderSpecified{}
	}

	//if there is a config update reload all modules
	if build.cache.configUpdate {
		moduleconfig := build.GetModuleConfig()
		buildmode := "build"
		if build.refreshEnabled {
			buildmode = "develop"
		}
		if moduleconfig.Config == nil {
			moduleconfig.Config = make(map[string]map[string]interface{})
		}
		moduleconfig.Config["antibuild"] = map[string]interface{}{
			"build_mode": buildmode,
		}

		moduleContext, err := modules.LoadModules(build.GetModulesFolder(), moduleconfig, build.moduleContext, log)
		if err != nil {
			return err
		}

		build.moduleContext = moduleContext
		for _, path := range append(build.OPPLocations, build.GetOutputFolder()) {
			err = os.RemoveAll(path)
			if err != nil {
				return errors.ErrFailedRemoveFile{
					FileLocation:  path,
					DetailedError: err,
				}
			}
		}
		build.OPPLocations = make([]string, 0, len(moduleconfig.OPPs)+1)
		for _, opp := range moduleconfig.OPPs {
			path, _ := ioutil.TempDir("", opp.String())
			build.OPPLocations = append(build.OPPLocations, path)
		}
		build.OPPLocations = append(build.OPPLocations, build.GetOutputFolder())
	}
	buildOutput := build.OPPLocations[0]
	// copy static folder
	if build.GetStaticFolder() != "" {
		log.Debug("Copying static folder")

		info, err := os.Lstat(build.GetStaticFolder())
		if err != nil {
			return errors.ErrFailedToMoveStatic{
				DetailedError: err,
			}
		}

		err = internal.GenCopy(build.GetStaticFolder(), buildOutput, info)
		if err != nil {
			return errors.ErrFailedToMoveStatic{
				DetailedError: err,
			}
		}

		log.Debug("Finished copying static folder")
	}

	pagesC := site.DeepCopy(build.GetRootSite())
	sites, err := site.Unfold(build.moduleContext, pagesC, log)
	if err != nil {
		return err
	}

	updatedSites := make([]*site.Site, 0, len(sites))
	for i := range sites {

		cd, foundCache := build.cache.data[sites[i].Slug.String()]

		shouldChange := !foundCache || depChange(cd, sites[i]) || build.cache.configUpdate

		log.Debugf("for site %s; FoundCache %v, depchange %v, configUpdate %v", sites[i].Slug.String(), foundCache, depChange(cd, sites[i]), build.cache.configUpdate)
		cd.dependencies = sites[i].Dependencies

		var s *site.Site

		if build.cache.checkData && !shouldChange { //if its already true, then we dont have to check for this
			var err error
			s, err = site.Gather(sites[i], build.GetTemplateFolder(), log)
			if err != nil {
				return err
			}

			for k, v := range s.Data {
				if !reflect.DeepEqual(v, cd.site.Data[k]) {
					shouldChange = true
				}
			}
		}

		for _, path := range sites[i].Templates {
			path = filepath.Join(build.GetTemplateFolder(), path)
			abs, err := filepath.Abs(path)
			if err != nil {
				return errors.ErrFailedCacheInvalidation{}
			}
			if abs == build.cache.templateUpdate {
				log.Debug("Forcing change because for because of template " + abs)
				site.RemoveTemplate(path)
				shouldChange = true
			}
		}

		for _, data := range sites[i].Data {
			variable := data.LoaderArguments.String()
			log.Debugf("Data, Variable:%s, id: %v", variable, data.Loader)
			if variable == build.cache.dataLoaderUpdate.variable && data.Loader == build.cache.dataLoaderUpdate.id {
				log.Debug("Forcing change because for because of data " + variable + " for module: " + data.Loader.String())
				site.RemoveDataLoader(data.Loader, variable, nil)
				shouldChange = true
			}
		}

		if shouldChange {
			if s == nil {
				var err error
				s, err = site.Gather(sites[i], build.GetTemplateFolder(), log)
				if err != nil {
					return err
				}
			}
			cd.site = *s
			updatedSites = append(updatedSites, s)
		}

		cd.shouldRemove = false
		build.cache.data[sites[i].Slug.String()] = cd
	}
	modulesConfig := build.GetModuleConfig()
	if len(modulesConfig.SPPs) > 0 {
		err := site.PostProcess(build.moduleContext, &updatedSites, modulesConfig.SPPs, log)
		if err != nil {
			return err
		}
	}
	//re cant do this before doing modules. they expect (and need) a clean slug
	for _, v := range updatedSites {
		v.OutputFile = path.Join(buildOutput, v.OutputFile)
	}

	err = site.Execute(updatedSites, log)
	if err != nil {
		return err
	}

	for k, v := range build.cache.data {

		if !v.shouldRemove {
			v.shouldRemove = true
			build.cache.data[k] = v

		} else {
			err = os.Remove(path.Join(buildOutput, k))
			if err != nil {
				return err
			}

			err = removeTopEmptyDir(buildOutput, k)
			if err != nil {
				return err
			}

			delete(build.cache.data, k)
		}
	}

	log.Debugf("number of OPPs %d", len(modulesConfig.OPPs))
	for oppi, opp := range modulesConfig.OPPs {
		if k := build.moduleContext.GetOPP(opp); k != nil {
			input, _ := filepath.Abs(build.OPPLocations[oppi])
			output, _ := filepath.Abs(build.OPPLocations[oppi+1])
			err := k.Process(input, output)
			if err != nil {
				return err
			}
			log.Debugf("OPP'ed using %s", opp.String())
		} else {
			log.Fatalf("Failed to load OPP: %s", opp.String())
		}
	}

	log.Infof("Built %d pages", len(updatedSites))
	log.Infof("Completed in %s", time.Since(start))

	//the true state will need to be checked during the process so we leave them true until the end
	build.cache.configUpdate = false
	build.cache.checkData = false
	build.cache.templateUpdate = ""

	return nil
}

func depChange(cd cacheData, s site.ConfigSite) bool {
	if len(s.Dependencies) != len(cd.dependencies) {
		return true
	}
	for i, d := range s.Dependencies {
		if d != cd.dependencies[i] {
			return true
		}
	}
	return false
}

func removeTopEmptyDir(base, rel string) error {
	var p = path.Join(base, rel)
	for {
		p, _ = path.Split(p)
		p = p[:len(p)-1]
		f, err := os.Open(p)
		if err != nil {
			return nil //no issue if you cant open the file, it might just be already removed
		}

		defer f.Close()

		// read in ONLY one file
		_, err = f.Readdir(1)

		if err != io.EOF {
			return os.RemoveAll(p)
		}
	}
}
