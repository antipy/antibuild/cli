// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site

import (
	"bufio"
	"os"
	"path/filepath"

	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

//Execute the templates of a []Site into the final files
func Execute(sites []*Site, log *logger.Logger) error {
	return execute(sites, log)
}

func execute(sites []*Site, log *logger.Logger) error {
	//export every template
	for _, site := range sites {
		log.Debugf("Building page for %s", site.OutputFile)

		err := executeTemplate(site)
		if err != nil {
			return err
		}
	}
	return nil
}

func executeTemplate(site *Site) error {
	//check all folders in the path of the output file
	err := os.MkdirAll(filepath.Dir(site.OutputFile), 0766)
	if err != nil {
		return errors.ErrFailedCreateFolder{
			Path:          site.OutputFile,
			DetailedError: err,
		}
	}

	//create the file
	file, err := os.Create(site.OutputFile)
	if err != nil {
		return errors.ErrFailedCreateFile{
			FileLocation:  site.OutputFile,
			DetailedError: err,
		}
	}
	defer file.Close() // we have to do this otherwise antibuild silently crashes on NTFS filesystems
	writer := bufio.NewWriter(file)
	//fill the file by executing the template
	err = globalTemplates[site.Template].ExecuteTemplate(writer, "html", site.Data)
	if err != nil {
		return errors.ErrFailedToExecuteTemplate{
			OutputPath:    site.OutputFile,
			DetailedError: err,
		}
	}
	writer.Flush()
	return nil
}
