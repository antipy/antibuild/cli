// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site

import (
	"bytes"
	"strings"

	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (
	// Data is info about data from the config
	Data struct {
		ShouldRange     string
		Loader          modules.ModuleFunctionID
		LoaderArguments IteratorString
		Parser          modules.ModuleFunctionID
		ParserArguments IteratorString
		PostProcessors  []DataPostProcessor
	}

	// DataPostProcessor is info about a data post processor from the config
	DataPostProcessor struct {
		PostProcessor          modules.ModuleFunctionID
		PostProcessorArguments IteratorString
	}

	// IteratorString is a data structuer representing an iterator string.
	// You can set variables by using .SetVariable
	IteratorString struct {
		Fields []iteratorField
	}
	iteratorField struct {
		Prefix   string
		Value    string
		Variable string
	}
)

// NewIteratorString returns a new IteratorString
func NewIteratorString(iteratorData string) (IteratorString, error) {
	ret := IteratorString{}
	err := ret.FromBytes([]byte(iteratorData))
	if err != nil {
		return IteratorString{}, err
	}
	return ret, nil
}

// SetVariable sets the variable in an IteratorString
func (is *IteratorString) SetVariable(variable, value string) {
	for i, f := range is.Fields {
		if f.Variable == variable {
			f.Value = value
			is.Fields[i] = f
		}
	}
}

// Prepend adds a string before the iterator string
func (is *IteratorString) Prepend(s string) {
	if len(is.Fields) >= 1 {
		is.Fields[0].Prefix = s + is.Fields[0].Prefix
	} else {
		is.Fields = append(is.Fields, iteratorField{
			Prefix: s,
		})
	}
}

// String turns the IteratorString into a string
func (is IteratorString) String() string {
	var builder strings.Builder

	//just to avoid aditional allocation
	size := 0
	for _, f := range is.Fields {
		size += len(f.Prefix)
		size += len(f.Value)
	}

	builder.Grow(size)

	for _, f := range is.Fields {
		builder.WriteString(f.Prefix)
		builder.WriteString(f.Value)
	}

	return builder.String()
}

// DefinitionString turns the iterator into the orignal definition string
func (is IteratorString) DefinitionString() string {
	var builder strings.Builder

	//just to avoid aditional allocation
	size := 0
	for _, f := range is.Fields {
		size += len(f.Prefix)
		if f.Variable != "" {
			size += len(f.Variable) + 4
		}
	}

	builder.Grow(size)

	for _, f := range is.Fields {
		builder.WriteString(f.Prefix)
		if f.Variable != "" {
			builder.WriteString("{{" + f.Variable + "}}")
		}
	}

	return builder.String()
}

// numUnsetVars gives you the amount of used variables
func (is *IteratorString) numUnsetVars() int {
	ret := 0
	for _, v := range is.Fields {
		if v.Variable != "" && v.Value == "" {
			ret++
		}
	}
	return ret
}

// UnsetVars gives you the used vars that havnt been set in this IteratorString,
// there could be duplicates is a variable is used multiple times
func (is *IteratorString) UnsetVars() []string {
	ret := make([]string, 0, len(is.Fields))
	for _, v := range is.Fields {
		if v.Variable != "" && v.Value == "" {
			ret = append(ret, v.Variable)
		}
	}
	return ret
}

// FromBytes creates an IteratorString from bytes.
// the input can only contain "{{" and "}}" as variable definitions
// if iteratorData is nil, it will do nothing and return
func (is *IteratorString) FromBytes(iteratorData []byte) error {
	if iteratorData == nil {
		return nil
	}
	//TODO: instead of using bytes.count we can do this ourselves so we can get better error reporting
	left, right := bytes.Count(iteratorData, []byte("{{")), bytes.Count(iteratorData, []byte("}}"))
	if left != right {
		overPopulated := "{{"
		if right > left {
			overPopulated = "}}"
		}
		return errors.ErrIteratorStringParsing{
			Source:        string(iteratorData),
			OverPopulated: overPopulated,
		}
	}
	fields := make([]iteratorField, 0, left)
	for i := 0; i < left; i++ {
		var f iteratorField
		i1 := bytes.Index(iteratorData, []byte("{{"))
		i2 := bytes.Index(iteratorData, []byte("}}"))

		f.Variable = string(iteratorData[i1+2 : i2])
		f.Prefix = string(iteratorData[:i1])

		fields = append(fields, f)

		//aka we just remove the parts we just used so that we can continue in a loop
		iteratorData = iteratorData[i2+2:]
	}

	if len(iteratorData) != 0 {
		f := iteratorField{
			Prefix:   string(iteratorData),
			Value:    "",
			Variable: "",
		}
		fields = append(fields, f)
	}

	is.Fields = fields
	return nil
}

// DeepCopy deepcopies the iteratorString
func (is IteratorString) DeepCopy() IteratorString {
	newIterator := IteratorString{}
	newIterator.Fields = make([]iteratorField, len(is.Fields))
	copy(newIterator.Fields, is.Fields)
	return newIterator
}

// MarshalJSON marshalls the data
func (is *IteratorString) MarshalJSON() ([]byte, error) {
	return []byte("\"" + is.DefinitionString() + "\""), nil
}

// UnmarshalJSON unmarshalls the data
func (is *IteratorString) UnmarshalJSON(data []byte) error {
	return is.FromBytes(data[1 : len(data)-1])
}

// MarshalJSON marshalls the data
func (d *Data) MarshalJSON() ([]byte, error) {
	return []byte("\"" + d.String() + "\""), nil
}

func (d *Data) String() string {
	out := ""

	if d.ShouldRange != "" {
		out += "[range:" + d.ShouldRange + "]"
	}

	out += "[" + d.Loader.String()
	if len(d.LoaderArguments.Fields) != 0 {
		out += ":" + d.LoaderArguments.DefinitionString()
	}
	out += "]"

	out += "[" + d.Parser.String()
	if len(d.ParserArguments.Fields) != 0 {
		out += ":" + d.ParserArguments.DefinitionString()
	}
	out += "]"

	for _, dpp := range d.PostProcessors {
		out += "[" + dpp.PostProcessor.String()
		if len(dpp.PostProcessorArguments.Fields) != 0 {
			out += ":" + dpp.PostProcessorArguments.DefinitionString()
		}
		out += "]"
	}

	return out
}

// UnmarshalJSON unmarshalls the data
func (d *Data) UnmarshalJSON(data []byte) error {
	data = data[1 : len(data)-1]

	data, loaderData := getNextPiece(data)
	{
		//get all the arguments for the loader
		sep := bytes.Split(loaderData, []byte(":"))
		if len(sep) == 0 {
			return errors.ErrFailedToParseDataString{
				DataString: string(loaderData),
				Location:   "data loader",
			}
		}

		if bytes.EqualFold(sep[0], []byte("range")) {
			if len(sep) < 2 { //only if bigger than 2 this is available
				return errors.ErrDataStringMissingRangeVariable{
					DataString: string(loaderData),
				}
			}

			d.ShouldRange = string(sep[1])

			//clean up the range
			data, loaderData = getNextPiece(data)
			//get all the arguments for the loader
			sep = bytes.Split(loaderData, []byte(":"))
			if len(sep) == 0 {
				return errors.ErrFailedToParseDataString{
					DataString: string(loaderData),
					Location:   "data loader variable",
				}
			}
		}

		d.Loader.FromBytes(sep[0])

		if len(sep) >= 2 { //only if bigger than 2 this is available
			err := d.LoaderArguments.FromBytes(sep[1])
			if err != nil {
				return err
			}
		} else {
			err := d.LoaderArguments.FromBytes(nil) //this should never return an error.
			if err != nil {
				return err
			}
		}
	}

	data, parserData := getNextPiece(data)

	{
		//get all the arguments for the dataParser
		sep := bytes.Split(parserData, []byte(":"))
		if len(sep) == 0 {
			return errors.ErrFailedToParseDataString{
				DataString: string(parserData),
				Location:   "data parser",
			}
		}

		d.Parser.FromBytes(sep[0])

		if len(sep) >= 2 { //only if bigger than 2 this is available
			err := d.ParserArguments.FromBytes(sep[1])
			if err != nil {
				return err
			}
		} else {
			err := d.ParserArguments.FromBytes(nil) //this should never return an error.
			if err != nil {
				return err
			}
		}

	}
	var postProcessorData []byte
	for len(data) > 0 {
		data, postProcessorData = getNextPiece(data)

		{
			var dpp = DataPostProcessor{}

			//get all the arguments for the dataPostProcessor
			sep := bytes.Split(postProcessorData, []byte(":"))
			if len(sep) == 0 {
				return errors.ErrFailedToParseDataString{
					DataString: string(postProcessorData),
					Location:   "data post processor",
				}
			}

			dpp.PostProcessor.FromBytes(sep[0])
			if len(sep) >= 2 { //only if bigger than 2 this is available
				err := dpp.PostProcessorArguments.FromBytes(sep[1])
				if err != nil {
					return err
				}
			} else {
				err := dpp.PostProcessorArguments.FromBytes(nil) //this should never return an error.
				if err != nil {
					return err
				}
			}

			d.PostProcessors = append(d.PostProcessors, dpp)
		}
	}

	return nil
}

// DeepCopy deepcopies the iteratorString
func (d Data) DeepCopy() Data {
	npp := make([]DataPostProcessor, len(d.PostProcessors))
	for i, opp := range d.PostProcessors {
		npp[i].PostProcessor = opp.PostProcessor
		npp[i].PostProcessorArguments = opp.PostProcessorArguments.DeepCopy()
	}
	d.PostProcessors = npp
	d.LoaderArguments = d.LoaderArguments.DeepCopy()
	d.ParserArguments = d.ParserArguments.DeepCopy()
	return d
}

func getNextPiece(data []byte) ([]byte, []byte) {
	//get the data from for the dataLoader
	i1 := bytes.IndexByte(data, '[')
	i2 := bytes.IndexByte(data, ']')

	piece := data[i1+1 : i2]
	data = data[i2+1:] //data is used for parser

	return data, piece
}
