// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site_test

import (
	"encoding/json"
	"html/template"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"

	"github.com/jaicewizard/tt"

	"gitlab.com/antipy/antibuild/cli/engine/site"
)

type unfoldPair struct {
	in    site.ConfigSite
	out   []*site.Site
	res   []*site.Site
	files map[string]string
}

type loader struct {
	data map[string][]byte
}

type parser struct{}

type iterator struct{}

type modulehost struct{}

var (
	log = logger.New(false, "") //aka dont log
)

func init() {
	os.MkdirAll(tempDir, 0777)
	err := ioutil.WriteFile(tempDir+"/t1", []byte("{{define \"html\"}}hello darkness my old friend{{end}}"), 0777)
	if err != nil {
		panic(err)
	}
}

func (mh modulehost) GetFuncMap() template.FuncMap {
	return nil
}

func (mh modulehost) GetDataLoader(void modules.ModuleFunctionID) modules.DataLoader {
	files := make(map[string][]byte)
	files["1"] = []byte(`{"data":"nothing"}`)
	return loader{data: files}
}

func (mh modulehost) GetDataParser(void modules.ModuleFunctionID) modules.DataParser {
	return parser{}
}

func (mh modulehost) GetDataWatcher(void modules.ModuleFunctionID) modules.DataWatcher {
	return nil
}

func (mh modulehost) GetDataPostProcessor(void modules.ModuleFunctionID) modules.DPP {
	return nil
}

func (mh modulehost) GetSPP(void modules.ModuleFunctionID) modules.SPP {
	return nil
}

func (mh modulehost) GetIterator(void modules.ModuleFunctionID) modules.Iterator {
	return iterator{}
}

func (mh modulehost) GetOPP(void modules.ModuleFunctionID) modules.OPP {
	return nil
}

func newMfid(s string) modules.ModuleFunctionID {
	mid := modules.ModuleFunctionID{}
	mid.FromBytes([]byte(s))
	return mid
}

var (
	tempDir             = path.Join(os.TempDir(), "antibuildtest")
	loaderArgument1, _  = site.NewIteratorString("1")
	indexSlug, _        = site.NewIteratorString(tempDir + "/out/index.html")
	articleIndexSlug, _ = site.NewIteratorString(tempDir + "/out/{{article}}/index.html")
	iteratorsSlug, _    = site.NewIteratorString("/tmp/templates/iterators")
)

var unfoldTests = []unfoldPair{
	{
		in: site.ConfigSite{
			Slug: indexSlug,
			Data: []site.Data{
				{
					Loader:          newMfid("l"),
					LoaderArguments: loaderArgument1,
					Parser:          newMfid("p"),
				},
			},
			Templates: []string{
				tempDir + "/t1",
			},
		},
		out: []*site.Site{
			{
				OutputFile: tempDir + "/out/index.html",
				Data: tt.Data{
					"data": "nothing",
				},
			},
		},
		files: map[string]string{
			tempDir + "/out/index.html": "hello darkness my old friend",
		},
	},
	{
		in: site.ConfigSite{
			Iterators: map[string]site.IteratorData{
				"article": {
					Iterator: newMfid("ls"),
				},
			},
			Slug: articleIndexSlug,
			Data: []site.Data{
				{
					Loader:          newMfid("l"),
					LoaderArguments: loaderArgument1,
					Parser:          newMfid("p"),
				},
			},
			Templates: []string{
				tempDir + "/t1",
			},
		},
		out: []*site.Site{
			{
				OutputFile: tempDir + "/out/hello/index.html",
				Data: tt.Data{
					"data": "nothing",
				},
			},
			{
				OutputFile: tempDir + "/out/world/index.html",
				Data: tt.Data{
					"data": "nothing",
				},
			},
		},
		files: map[string]string{
			tempDir + "/out/index.html": "hello darkness my old friend",
		},
	},
}

func (l loader) GetPipe(f string) modules.Pipe {
	return func([]byte) ([]byte, error) {
		return l.data[f], nil
	}
}

func (p parser) GetPipe(useless string) modules.Pipe {
	return func(data []byte) ([]byte, error) {
		var jsonData map[string]interface{}

		err := json.Unmarshal(data, &jsonData)
		if err != nil {
			panic(err)
		}

		var retData = make(tt.Data, len(jsonData))
		for k, v := range jsonData {
			retData[k] = v
		}
		ret, err := retData.GobEncode()
		if err != nil {
			return nil, errors.ErrModuleFailedToExececutePipe{
				DetailedError: err,
			}
		}
		return ret, nil
	}
}

func (i iterator) GetIterations(location string) ([]string, error) {
	return []string{
		"hello",
		"world",
	}, nil
}

func (i iterator) GetPipe(variable string) modules.Pipe {
	return nil
}

//Testunfold doesn't test template parsing, if anything failed it will be done during execute
func TestUnfold(t *testing.T) {
	for _, test := range unfoldTests {
		in := site.DeepCopy(test.in)
		dat, err := site.Unfold(modulehost{}, in, log)
		if err != nil {
			t.Fatal(err.Error())
		}

		for _, d := range dat {
			s, err := site.Gather(d, "", log)
			if err != nil {
				t.Fatal(err.Error())
			}
			test.res = append(test.res, s)
		}
		if len(test.out) != len(test.res) {
			for _, v := range test.res {
				print("\n" + v.OutputFile)
			}
			t.FailNow()
		}
		for i := 0; i < len(test.res); i++ {
			if test.out[i].OutputFile != test.res[i].OutputFile {
				print("slug should be: "+test.out[i].OutputFile+" but is: "+test.res[i].OutputFile+" for ", i, "\n")
				for _, v := range test.res {
					print(v.OutputFile + "\n")
				}
				t.FailNow()
			}
		}

		for k := range test.out[0].Data {
			if test.out[0].Data[k] != test.res[0].Data[k] {
				t.FailNow()
			}
		}
	}
}

func TestExecute(t *testing.T) {
	for _, test := range unfoldTests {
		in := site.DeepCopy(test.in)
		dat, err := site.Unfold(modulehost{}, in, log)
		if err != nil {
			t.Fatal(err.Error())
		}

		for _, d := range dat {
			s, err := site.Gather(d, "", log)
			if err != nil {
				t.Fatal(err.Error())
			}

			test.res = append(test.res, s)
		}

		err = site.Execute(test.res, log)
		if err != nil {
			t.Fatal(err.Error())
		}

		for file, data := range test.files {
			dat, err := ioutil.ReadFile(file)
			if err != nil {
				t.Fatal(err.Error())
			}

			if string(dat) != data {
				t.FailNow()
			}
		}
	}
}

var benchMarks = [...]site.ConfigSite{
	{
		Slug: indexSlug,
		Data: []site.Data{
			{
				Loader:          newMfid("l"),
				LoaderArguments: loaderArgument1,
				Parser:          newMfid("p"),
			},
		},
		Templates: []string{
			"t1",
		},
	},
	{
		Iterators: map[string]site.IteratorData{
			"article": {
				Iterator:          newMfid("ls"),
				IteratorArguments: iteratorsSlug,
			},
		},
		Slug: articleIndexSlug,
		Data: []site.Data{
			{
				Loader:          newMfid("l"),
				LoaderArguments: loaderArgument1,
				Parser:          newMfid("p"),
			},
		},
		Templates: []string{
			"t1",
		},
	},
}

func BenchmarkUnfold(b *testing.B) {
	b.Run("simple-basic", genUnfold(0))
	b.Run("simple-iterator", genUnfold(1))
}

func genUnfold(benchID int) func(*testing.B) {
	return func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			s := site.DeepCopy(benchMarks[benchID])
			_, err := site.Unfold(modulehost{}, s, log)
			if err != nil {
				b.Fatal(err.Error())
			}
		}
	}
}

func BenchmarkGather(b *testing.B) {
	b.Run("simple-basic", genGather(0))
	b.Run("simple-iterator", genGather(1))
}

func genGather(benchID int) func(*testing.B) {
	return func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			s := site.DeepCopy(benchMarks[benchID])

			_, err := site.Unfold(modulehost{}, s, log)
			if err != nil {
				b.Fatal(err.Error())
			}
		}

		var sites = make([][]site.ConfigSite, b.N)
		for n := 0; n < b.N; n++ {
			s := site.DeepCopy(benchMarks[benchID])
			sites[n], _ = site.Unfold(modulehost{}, s, log)
		}

		b.ResetTimer()
		for n := 0; n < b.N; n++ {
			for _, d := range sites[n] {
				_, err := site.Gather(d, tempDir, log)
				if err != nil {
					b.Fatal(err.Error())
				}
			}
		}
	}
}
