// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site

import (
	"fmt"
	"reflect"
	"testing"
)

type (
	stringIteratorPair struct {
		s                    string
		iterator             IteratorString
		tostring             string
		setVariables         []variablepair
		postVariableIterator IteratorString
		numVars              int
		postVariableNumVars  int
	}
	variablepair struct {
		variable string
		value    string
	}
)

var (
	iteratorsStringTests = []stringIteratorPair{
		{
			s: "data/articles/{{article}}",
			iterator: IteratorString{
				Fields: []iteratorField{
					{
						Prefix:   "data/articles/",
						Value:    "",
						Variable: "article",
					},
				},
			},
			tostring: "data/articles/",
			setVariables: []variablepair{
				{
					variable: "article",
					value:    "something",
				},
			},
			postVariableIterator: IteratorString{
				Fields: []iteratorField{
					{
						Prefix:   "data/articles/",
						Value:    "something",
						Variable: "article",
					},
				},
			},
			numVars:             1,
			postVariableNumVars: 0,
		},
		{
			s: "data/articles/{{article}}/meta.yaml",
			iterator: IteratorString{
				Fields: []iteratorField{
					{
						Prefix:   "data/articles/",
						Value:    "",
						Variable: "article",
					},
					{
						Prefix:   "/meta.yaml",
						Value:    "",
						Variable: "",
					},
				},
			},
			tostring: "data/articles//meta.yaml",
			setVariables: []variablepair{
				{
					variable: "article",
					value:    "something",
				},
			},
			postVariableIterator: IteratorString{
				Fields: []iteratorField{
					{
						Prefix:   "data/articles/",
						Value:    "something",
						Variable: "article",
					},
					{
						Prefix:   "/meta.yaml",
						Value:    "",
						Variable: "",
					},
				},
			},
			numVars:             1,
			postVariableNumVars: 0,
		},
	}
)

//TestIteratorsToString tests simple iterator string to string parsing checking if the results are correct
func TestIteratorsToString(t *testing.T) {
	for _, test := range iteratorsStringTests {
		if test.iterator.String() != test.tostring {
			t.FailNow()
		}
	}
}

func TestIteratorsFromBytes(t *testing.T) {
	for _, test := range iteratorsStringTests {
		var iterator IteratorString
		err := iterator.FromBytes([]byte(test.s))
		if err != nil {
			t.FailNow()
		}
		if !reflect.DeepEqual(iterator, test.iterator) {
			fmt.Println(iterator)
			fmt.Println(test.iterator)
			t.FailNow()
		}
	}
}

func TestIteratorsFromString(t *testing.T) {
	for _, test := range iteratorsStringTests {
		var iterator IteratorString
		err := iterator.FromBytes([]byte(test.s))
		if err != nil {
			t.FailNow()
		}
		if !reflect.DeepEqual(iterator, test.iterator) {
			fmt.Println(iterator)
			fmt.Println(test.iterator)
			t.FailNow()
		}
	}
}

func TestIteratorsSetVariable(t *testing.T) {
	for _, test := range iteratorsStringTests {
		it := test.iterator.DeepCopy()
		for _, variable := range test.setVariables {
			it.SetVariable(variable.variable, variable.value)
		}
		if !reflect.DeepEqual(it, test.postVariableIterator) {
			fmt.Println(it)
			fmt.Println(test.postVariableIterator)
			t.FailNow()
		}
	}
}

func TestIteratorsGetNumVars(t *testing.T) {
	for _, test := range iteratorsStringTests {
		it := test.iterator.DeepCopy()
		for _, variable := range test.setVariables {
			it.SetVariable(variable.variable, variable.value)
		}
		if !reflect.DeepEqual(it, test.postVariableIterator) {
			fmt.Println(it)
			fmt.Println(test.postVariableIterator)
			t.FailNow()
		}
	}
}
