// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site

import (
	"bytes"

	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	// IteratorData is info about an iterator from the config
	IteratorData struct {
		Iterator          modules.ModuleFunctionID
		IteratorArguments IteratorString
		List              []string
	}
)

// MarshalJSON marshalls the iterator data
func (i IteratorData) MarshalJSON() ([]byte, error) {
	return []byte("\"" + i.String() + "\""), nil
}

func (i IteratorData) String() string {
	out := ""

	out += "[" + i.Iterator.String()
	if len(i.IteratorArguments.Fields) != 0 {
		out += ":" + i.IteratorArguments.DefinitionString()
	}
	out += "]"

	return out
}

// UnmarshalJSON unmarshalls the iterator data
func (i *IteratorData) UnmarshalJSON(data []byte) error {
	//get the data from for the dataLoader
	i1 := bytes.IndexByte(data, '[')
	i2 := bytes.IndexByte(data, ']')

	iteratorData := data[i1+1 : i2]

	{
		//get all the arguments for the loader
		sep := bytes.Split(iteratorData, []byte(":"))
		if len(sep) == 0 {
			return errors.ErrIteratorStringParseFailed{
				String: string(iteratorData),
			}
		}

		i.Iterator.FromBytes(sep[0])
		if len(sep) >= 2 { //only if bigger than 2 this is available
			err := i.IteratorArguments.FromBytes(sep[1])
			if err != nil {
				return err
			}
		} else {
			err := i.IteratorArguments.FromBytes(nil) //this should never return an error.
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func unique(stringSlice []string) []string {
	keys := make(map[string]bool)
	list := make([]string, 0, len(stringSlice))

	for _, entry := range stringSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func replaceVarData(d Data, variable string, value string) Data {
	d.LoaderArguments.SetVariable(variable, value)
	d.ParserArguments.SetVariable(variable, value)

	for pp := range d.PostProcessors {
		d.PostProcessors[pp].PostProcessorArguments.SetVariable(variable, value)
	}
	return d
}

func numIncludedVars(cSite ConfigSite) int {
	usedVars := cSite.Slug.numUnsetVars()
	for _, v := range cSite.Data {
		//we cant get the length of the fields, this would result in one extra in cases with a postfix
		usedVars += v.LoaderArguments.numUnsetVars() + v.ParserArguments.numUnsetVars()
	}

	for _, v := range cSite.Iterators {
		usedVars += v.IteratorArguments.numUnsetVars()
	}

	return usedVars
}

func doIteratorVariables(cSite *ConfigSite) error {
	for i, v := range cSite.Iterators {
		vars := v.IteratorArguments.UnsetVars()
		for _, ivar := range vars {
			var value string
			var ok bool
			if value, ok = cSite.IteratorValues[ivar]; !ok {
				return errors.ErrDataStringMissingIterator{
					Iterator: ivar,
				}
			}
			v.IteratorArguments.SetVariable(ivar, value)
		}
		cSite.Iterators[i] = v
	}
	return nil
}

//DeepCopy deep copies the site.
func DeepCopy(cSite ConfigSite) ConfigSite {
	cSite.Slug = cSite.Slug.DeepCopy()

	nsd := make([]Data, len(cSite.Data))
	for k, v := range cSite.Data {
		nsd[k] = v.DeepCopy()
	}
	cSite.Data = nsd

	niv := make(map[string]string, len(cSite.IteratorValues))
	for k, v := range cSite.IteratorValues {
		niv[k] = v
	}
	cSite.IteratorValues = niv

	nid := make(map[string]IteratorData, len(cSite.Iterators))
	for k, v := range cSite.Iterators {
		nid[k] = IteratorData{
			Iterator:          v.Iterator,
			IteratorArguments: v.IteratorArguments.DeepCopy(),
		}
	}
	cSite.Iterators = nid

	ns := make([]ConfigSite, len(cSite.Sites))
	for i, v := range cSite.Sites {
		ns[i] = DeepCopy(v)
	}
	cSite.Sites = ns

	ndp := make([]string, len(cSite.Dependencies))
	copy(ndp, cSite.Dependencies)
	cSite.Dependencies = ndp

	return cSite
}

func doIterators(cSite ConfigSite, log *logger.Logger) ([]ConfigSite, error) {
	err := doIteratorVariables(&cSite)
	if err != nil {
		return nil, err
	}

	err = gatherIterators(cSite.moduleContext, cSite.Iterators)
	if err != nil {
		return nil, err
	}

	var usedIterators []string

	var newData []Data
	for _, d := range cSite.Data {
		if d.ShouldRange != "" {
			rangeVar := d.ShouldRange
			var i IteratorData
			var ok bool
			if i, ok = cSite.Iterators[rangeVar]; !ok {
				return nil, errors.ErrDataStringMissingIterator{
					Iterator: rangeVar,
				}
			}

			d.ShouldRange = ""

			for _, v := range i.List {
				nd := d.DeepCopy()
				nd = replaceVarData(nd, rangeVar, v)
				newData = append(newData, nd)
			}
			usedIterators = append(usedIterators, rangeVar)
		} else {
			//no range so wont be modified and doestn need copying
			newData = append(newData, d)
		}
	}

	cSite.Data = newData

	usedVars := cSite.Slug.UnsetVars()

	//these are the variables that are used inside the site and should be enough
	usedIterators = unique(append(usedIterators, usedVars...))

	for _, d := range cSite.Data {
		usedVars = append(usedVars, d.ParserArguments.UnsetVars()...)
		usedVars = append(usedVars, d.LoaderArguments.UnsetVars()...)
		for _, v := range d.PostProcessors {
			usedVars = append(usedVars, v.PostProcessorArguments.UnsetVars()...)
		}
	}

	usedVars = unique(usedVars)

	log.Debugf("found %d variables to range over: %v", len(usedVars), usedVars)
	options := make([][]string, len(usedVars))

	for i, iOpts := range usedVars {
		var ok bool
		var iter IteratorData
		if iter, ok = cSite.Iterators[iOpts]; !ok {
			return nil, errors.ErrDataStringMissingIterator{
				Iterator: iOpts,
			}
		}
		options[i] = iter.List
		if len(options[i]) == 0 {
			var value string

			if value, ok = cSite.IteratorValues[iOpts]; !ok {
				//The iterator is defined, but didnt yield any values
				log.Info("Iterator " + iOpts + " is defined, but did not yield any value")
				return []ConfigSite{}, nil
			}

			options[i] = []string{value}
		}
	}

	for _, v := range usedIterators {
		iteratorValue := cSite.Iterators[v]
		cSite.Dependencies = append(cSite.Dependencies, iteratorValue.Iterator.String()+iteratorValue.IteratorArguments.String()) //do iteratorArguments later
		delete(cSite.Iterators, v)
	}

	olen := 1
	for _, iOpts := range options {
		olen *= len(iOpts)
	}

	log.Debugf("Found %d values for the iterators: %v", olen, options)

	var sites = make([]ConfigSite, olen)
	sites[0] = cSite
	lastUpperBound := 1
	for vi, iOpts := range options {
		variable := usedVars[vi]
		for opti, value := range iOpts {

			base := (lastUpperBound) * opti
			for i := 0; i < lastUpperBound; i++ {
				sites[base+i] = DeepCopy(sites[i])
			}

			for i := base; i < base+lastUpperBound; i++ { //every variable gets the lastUpperBound  amount of copies
				sites[i].Slug.SetVariable(variable, value)
				for di, d := range sites[i].Data {
					sites[i].Data[di] = replaceVarData(d, variable, value)
				}
				if sites[i].IteratorValues == nil {
					sites[i].IteratorValues = make(map[string]string)
				}
				sites[i].IteratorValues[variable] = value
			}
		}
		lastUpperBound *= len(iOpts)
	}

	return sites, nil
}
