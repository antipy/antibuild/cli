// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package site handles everything that has to do with the final site.
// This package loads the data, gathers the templates and renders it out.
// The site package is basically the glue between all the components.
package site

import (
	"html/template"
	"text/template/parse"

	apiSite "gitlab.com/antipy/antibuild/api/site"
	"gitlab.com/antipy/antibuild/cli/engine/modules"

	"github.com/jaicewizard/tt"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

type (
	//Site is the way a site is defined after all of its data and templates have been collected
	Site struct {
		OutputFile string
		Template   string
		Data       tt.Data
	}

	//ConfigSite is the way a site is defined in the config file.
	//Template paths must be relative to CWD or absolute
	ConfigSite struct {
		Iterators      map[string]IteratorData `json:"iterators,omitempty"`
		Slug           IteratorString          `json:"slug,omitempty"`
		Templates      []string                `json:"templates,omitempty"`
		Data           []Data                  `json:"data,omitempty"`
		Sites          []ConfigSite            `json:"sites,omitempty"`
		IteratorValues map[string]string       `json:"-"`
		Dependencies   []string                `json:"-"`
		moduleContext  modulefuncGetter
	}

	modulefuncGetter interface {
		GetFuncMap() template.FuncMap
		GetDataWatcher(modules.ModuleFunctionID) modules.DataWatcher
		GetDataLoader(modules.ModuleFunctionID) modules.DataLoader
		GetDataParser(modules.ModuleFunctionID) modules.DataParser
		GetDataPostProcessor(modules.ModuleFunctionID) modules.DPP
		GetSPP(modules.ModuleFunctionID) modules.SPP
		GetIterator(modules.ModuleFunctionID) modules.Iterator
		GetOPP(modules.ModuleFunctionID) modules.OPP
	}

	pipelineCacheID struct {
		id       modules.ModuleFunctionID
		variable string
		input    string //This is actually a []byte, but slices cant go in map keys :(
	}
)

var (
	globalTemplates = make(map[string]*template.Template)
	subTemplates    = make(map[string]string)

	pipelineCache = make(map[pipelineCacheID][]byte)
)

/*
	UNFOLD

	Parse the tree into the individual sites by iterating over the children of parents
	and combining their data until only sites with no more the children remain. Add
	these sites to an array, so there is no more nesting.
	Parse the sites tree from the config file, any final site (no child sites) will
	be parsed into the final list of sites.
*/

//Unfold the ConfigSite into a []ConfigSite
func Unfold(moduleContext modulefuncGetter, configSite ConfigSite, log *logger.Logger) ([]ConfigSite, error) {
	var sites []ConfigSite
	globalTemplates = make(map[string]*template.Template, len(sites))
	configSite.moduleContext = moduleContext
	err := unfold(configSite, nil, &sites, log)
	if err != nil {
		return sites, err
	}

	return sites, nil
}

func unfold(cSite ConfigSite, parent *ConfigSite, sites *[]ConfigSite, log *logger.Logger) error {
	if parent != nil {
		mergeConfigSite(&cSite, parent)
	}

	log.Debugf("Unfolding %s", cSite.Slug)

	numIncludedVars := numIncludedVars(cSite)
	log.Debugf("IncludedVars: %d", numIncludedVars)
	log.Debugf("Children: %d", len(cSite.Sites))

	//If this is the last in the chain, add it to the list of return values
	if len(cSite.Sites) == 0 && numIncludedVars == 0 {
		for _, v := range cSite.Data {
			dep := v.Loader.String() +
				v.LoaderArguments.String() +
				v.Parser.String() +
				v.ParserArguments.String()
			for _, pp := range v.PostProcessors {
				dep += pp.PostProcessor.String() + pp.PostProcessorArguments.String()
			}
			cSite.Dependencies = append(cSite.Dependencies, dep)
		}

		//append site to the list of sites that will be executed
		(*sites) = append(*sites, cSite)
		log.Debug("Unfolded to final site")

		return nil
	}

	if numIncludedVars > 0 {
		log.Debugf("Doing iterators")
		itSites, err := doIterators(cSite, log)
		if err != nil {
			return err
		}

		for i := range itSites {
			err := unfold(itSites[i], nil, sites, log)
			if err != nil {
				return err
			}
		}

		return nil
	}

	log.Debugf("Gathering iterators")
	// we might not have iterators values that we need right now
	// but if we can parse them now then we dont need duplicate work
	err := gatherIterators(cSite.moduleContext, cSite.Iterators)
	if err != nil {
		return err
	}

	for _, childSite := range cSite.Sites {
		err = unfold(childSite, &cSite, sites, log)
		if err != nil {
			return err
		}
	}

	return nil
}

//mergeConfigSite merges the src into the dst
func mergeConfigSite(dst *ConfigSite, src *ConfigSite) {
	if dst.Data != nil {
		dst.Data = append(dst.Data, src.Data...) // just append
	} else {
		dst.Data = make([]Data, len(src.Data)) // or make a new one and fill it
		copy(dst.Data, src.Data)
	}

	if dst.Templates != nil {
		dst.Templates = append(dst.Templates, src.Templates...) // just append
	} else {
		dst.Templates = make([]string, len(src.Templates)) // or make a new one and fill it
		copy(dst.Templates, src.Templates)
	}

	if dst.Dependencies != nil {
		dst.Dependencies = append(dst.Dependencies, src.Dependencies...) // just append
	} else {
		dst.Dependencies = make([]string, len(src.Dependencies)) // or make a new one and fill it
		copy(dst.Dependencies, src.Dependencies)
	}

	if dst.IteratorValues == nil {
		dst.IteratorValues = make(map[string]string, len(src.IteratorValues)) // if none exist, make a new one to fill
	}

	for i, s := range src.IteratorValues {
		dst.IteratorValues[i] = s
	}

	if dst.Iterators == nil {
		dst.Iterators = make(map[string]IteratorData, len(src.Iterators)) // if none exist, make a new one to fill
	}

	for i, s := range src.Iterators {
		dst.Iterators[i] = s
	}
	slug := src.Slug.DeepCopy()
	slug.Fields = append(slug.Fields, dst.Slug.Fields...)
	dst.Slug = slug

	dst.moduleContext = src.moduleContext
}

// RemoveTemplate cleans a template from the template cache
func RemoveTemplate(path string) {
	delete(subTemplates, path)
}

// RemoveDataLoader cleans a dataloader from the cache
func RemoveDataLoader(id modules.ModuleFunctionID, variable string, input []byte) {
	delete(pipelineCache, pipelineCacheID{
		id:       id,
		variable: variable,
		input:    string(input),
	})
}

// PostProcess all sites
func PostProcess(moduleContext *modules.ModuleContext, sites *[]*Site, spps []modules.ModuleFunctionID, log *logger.Logger) error {
	send := make([]apiSite.Site, 0, len(*sites))
	var recieve []apiSite.Site

	for _, d := range *sites {
		send = append(send, apiSite.Site{
			Slug:     d.OutputFile,
			Template: d.Template,
			Data:     d.Data,
		})
	}

	line := make([]modules.Pipe, 0, len(spps))
	for _, spp := range spps {
		if k := moduleContext.GetSPP(spp); k != nil {
			line = append(line, k.GetPipe(""))
		}
	}

	bytes, err := modules.ExecPipeline(apiSite.Encode(send), line...)
	if err != nil {
		return err
	}

	recieve = apiSite.Decode(bytes)
	*sites = make([]*Site, 0, len(recieve))

	for _, d := range recieve {
		*sites = append(*sites, &Site{
			OutputFile: d.Slug,
			Template:   d.Template,
			Data:       d.Data,
		})
	}

	return nil
}

//GetTemplateTree gets the template according to
func GetTemplateTree(template string) *parse.Tree {
	if v, ok := globalTemplates[template]; ok {
		return v.Tree
	}
	return nil
}
