// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package site

import (
	"html/template"
	"io/ioutil"

	"path/filepath"

	"github.com/jaicewizard/tt"
	"gitlab.com/antipy/antibuild/cli/engine/modules"
	"gitlab.com/antipy/antibuild/cli/internal"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
	"gitlab.com/antipy/antibuild/cli/internal/logger"
)

// Gather after unfolding
func Gather(cSite ConfigSite, templatePrefix string, log *logger.Logger) (*Site, error) {
	log.Debugf("Gathering information for %s", cSite.Slug)
	log.Debugf("Site data: %v", cSite)

	site := &Site{
		OutputFile: cSite.Slug.String(),
	}

	err := gatherData(cSite.moduleContext, site, cSite.Data, log)
	if err != nil {
		return nil, err
	}
	log.Debugf("Gathered data: %v", site.Data)

	err = gatherTemplates(cSite.moduleContext, site, templatePrefix, cSite.Templates, log)
	if err != nil {
		return nil, err
	}

	log.Debugf("Finished gathering")

	return site, nil
}

//collect data objects from modules
func gatherData(moduleContext modulefuncGetter, site *Site, files []Data, log *logger.Logger) error {
	for _, d := range files {

		//init data if it is empty
		if site.Data == nil {
			site.Data = make(map[interface{}]interface{})
		}

		var dataLoader modules.DataParser
		if dataLoader = moduleContext.GetDataLoader(d.Loader); dataLoader == nil {
			return errors.ErrUsingUnloadedModule{
				Identifier: d.Loader.String(),
			}
		}

		fPipe := func(b []byte) ([]byte, error) {
			if cache, ok := pipelineCache[pipelineCacheID{
				id:       d.Loader,
				variable: d.LoaderArguments.String(),
			}]; ok && moduleContext.GetDataWatcher(d.Loader) != nil {
				return cache, nil
			} else {
				data, err := dataLoader.GetPipe(d.LoaderArguments.String())(b)
				pipelineCache[pipelineCacheID{
					id:       d.Loader,
					variable: d.LoaderArguments.String(),
				}] = data
				return data, err
			}
		}

		var dataParser modules.DataParser
		if dataParser = moduleContext.GetDataParser(d.Parser); dataParser == nil {
			return errors.ErrUsingUnloadedModule{
				Identifier: d.Parser.String(),
			}
		}

		pPipe := func(b []byte) ([]byte, error) {
			if cache, ok := pipelineCache[pipelineCacheID{
				id:       d.Parser,
				variable: d.ParserArguments.String(),
				input:    string(b),
			}]; ok {
				return cache, nil
			} else {
				data, err := dataParser.GetPipe(d.ParserArguments.String())(b)
				pipelineCache[pipelineCacheID{
					id:       d.Parser,
					variable: d.ParserArguments.String(),
					input:    string(b),
				}] = data
				return data, err
			}
		}

		var ppPipes []modules.Pipe
		var validPPPipes = 0
		for _, dpp := range d.PostProcessors {
			dpp := dpp
			var dppm modules.DPP
			if dppm = moduleContext.GetDataPostProcessor(dpp.PostProcessor); dppm == nil {
				return errors.ErrUsingUnloadedModule{
					Identifier: dpp.PostProcessor.String(),
				}
			}

			ppPipes = append(ppPipes, func(b []byte) ([]byte, error) {
				if cache, ok := pipelineCache[pipelineCacheID{
					id:       d.Parser,
					variable: dpp.PostProcessorArguments.String(),
					input:    string(b),
				}]; ok {
					return cache, nil
				} else {
					data, err := dppm.GetPipe(dpp.PostProcessorArguments.String())(b)
					pipelineCache[pipelineCacheID{
						id:       d.Parser,
						variable: dpp.PostProcessorArguments.String(),
						input:    string(b),
					}] = data
					return data, err
				}
			})
			if ppPipes != nil {
				validPPPipes++
			}
		}

		var pipes = []modules.Pipe{
			fPipe,
			pPipe,
		}

		pipes = append(pipes, ppPipes...)

		bytes, err := modules.ExecPipeline(nil, pipes...)
		if err != nil {
			return err
		}

		var data tt.Data

		err = data.GobDecode(bytes)
		if err != nil {
			return err
		}
		log.Debugf("Gathered data for file %s: %v", d.LoaderArguments, data)

		deepMergeMap(site.Data, data)
	}

	return nil
}

func gatherTemplates(modulesContext modulefuncGetter, site *Site, templateFolder string, templates []string, log *logger.Logger) error {
	functions := modulesContext.GetFuncMap()
	log.Debugf("Setting functions: %v", functions)

	finalTemplate := template.New("").Funcs(functions)

	for _, tPath := range templates {
		//prefix the templates with the templateFolder
		path := filepath.Join(templateFolder, tPath)
		if _, ok := subTemplates[path]; !ok {
			log.Debug("Reading template file: " + path)
			bytes, err := ioutil.ReadFile(path)
			if err != nil {
				return errors.ErrFailedOpenFile{
					FileLocation:  path,
					DetailedError: err,
				}
			}
			subTemplates[path] = string(bytes)
		}

		_, err := finalTemplate.Parse(subTemplates[path])
		if err != nil {
			return err
		}
	}
	//TODO: make the ID be the template path
	//get the template with the TemplateFunctions initialized
	id := internal.RandString(32)

	globalTemplates[id] = finalTemplate
	site.Template = id

	return nil
}

//collect iterators from modules skiping over any already set values for obvious reasons
func gatherIterators(moduleContext modulefuncGetter, iterators map[string]IteratorData) error {
	for n, i := range iterators {
		if len(i.List) != 0 {
			continue
		}

		var it modules.Iterator
		if it = moduleContext.GetIterator(i.Iterator); it == nil {
			return errors.ErrUsingUnloadedModule{
				Identifier: i.Iterator.String(),
			}
		}
		var err error
		i.List, err = it.GetIterations(i.IteratorArguments.String())
		if err != nil {
			return err
		}

		iterators[n] = i
	}

	return nil
}

func deepMergeMap(dst, src map[interface{}]interface{}) {
	for k, srcValue := range src {
		if dstValue, ok := dst[k]; ok { //if the value is alreay available then it should be merged properly
			//these are all the mergable types supported by tt, so we dont need to support anything else
			if dstMap, dOK := dstValue.(map[interface{}]interface{}); dOK {
				if srcMap, sOK := srcValue.(map[interface{}]interface{}); sOK {
					deepMergeMap(dstMap, srcMap)
					srcValue = dstValue //to make sure that we dont
				}
			} else if dstSlice, dOK := dstValue.([]interface{}); dOK {
				if srcSlice, sOK := srcValue.([]interface{}); sOK {
					dstSlice = append(dstSlice, srcSlice)
					srcValue = dstSlice
				}
			}
			//else, if the key is already set but not mergable then it should be overwritten
		}

		dst[k] = srcValue
	}
}
