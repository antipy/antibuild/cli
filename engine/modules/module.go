// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"bytes"
	"encoding/json"
	"strings"

	"github.com/blang/semver"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (
	// Modules is the part of the config file that handles modules
	Modules struct {
		Dependencies moduleInfo                        `json:"dependencies"`
		Config       map[string]map[string]interface{} `json:"config,omitempty"`
		SPPs         []ModuleFunctionID                `json:"spps,omitempty"`
		OPPs         []ModuleFunctionID                `json:"opps,omitempty"`
	}

	moduleInfo []Module
	// Module with info about the path and version
	Module struct {
		Name       string
		Repository string
		Version    semver.Version
	}
)

var (
	//LatestVersion is the defined version used to notate the latest version
	LatestVersion = semver.Version{
		Major: 0,
		Minor: 0,
		Patch: 0,
	}
)

// UnmarshalJSON on a module
func (m *Module) UnmarshalJSON(data []byte) error {
	data = bytes.Trim(data, "\"")
	err := m.fromBytes(data)
	if err != nil {
		return err
	}

	return nil
}

func (m *Module) fromBytes(data []byte) error {
	split := bytes.Split(data, []byte("@"))
	if len(split) < 1 || len(split) > 2 {
		return errors.ErrModuleStringParseFailed{
			String: string(data),
		}
	}
	if len(split[0]) == 0 {
		return errors.ErrModuleStringParseFailed{
			String: string(data),
		}
	}

	m.Repository = string(split[0])

	var v = LatestVersion
	if len(split) == 2 && string(split[1]) != "latest" {
		if len(split[1]) == 0 {
			return errors.ErrModuleStringParseFailed{
				String: string(data),
			}
		}

		var err error
		v, err = semver.ParseTolerant(string(split[1]))
		if err != nil {
			return errors.ErrModuleStringParseVersionFailed{
				String:        string(data),
				DetailedError: err,
			}
		}
	}
	m.Version = v

	return nil
}

func (m *Module) bytes() []byte {
	versionString := m.Version.String()
	vlen := 1 + len(versionString)
	res := make([]byte, 0, 1+len(m.Repository)+vlen+1)
	res = append(res, byte('"'))
	res = append(res, []byte(m.Repository)...)
	res = append(res, byte('@'))
	res = append(res, []byte(versionString)...)
	res = append(res, byte('"'))

	return res
}

// ParseModuleString for config and cli
func ParseModuleString(moduleString string) (m *Module, err error) {
	m = new(Module)

	err = m.fromBytes([]byte(strings.Trim(moduleString, "\"")))

	return
}

// MarshalJSON on a module
func (m Module) MarshalJSON() ([]byte, error) {
	return m.bytes(), nil
}

// UnmarshalJSON on the modules list
func (m *moduleInfo) UnmarshalJSON(data []byte) error {
	modules := make(map[string]Module)
	err := json.Unmarshal(data, &modules)
	if err != nil {
		return err
	}
	ms := make([]Module, 0, len(modules))
	for k, mod := range modules {
		mod.Name = k
		ms = append(ms, mod)
	}

	*m = moduleInfo(ms)

	return nil
}

// MarshalJSON on the modules list
func (m *moduleInfo) MarshalJSON() ([]byte, error) {
	ms := ([]Module)(*m)
	data := make(map[string]Module, len(ms))
	for _, v := range ms {
		data[v.Name] = v
	}
	return json.Marshal(data)
}
