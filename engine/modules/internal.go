// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"bufio"
	"io"

	"github.com/blang/semver"
)

type (
	//InternalModule is a module that is compiled inside antibuild itself
	InternalModule struct {
		Start      func(io.Reader, io.Writer)
		Version    semver.Version
		Repository string
	}
	internalModuleMap struct {
		set     bool
		modules map[string]InternalModule
	}
)

const (
	//HaveSameVersion is for when you have the same version as internal module
	HaveSameVersion = iota
	//HaveHigherVersion is for when you have a higher version as internal module
	HaveHigherVersion
	//HaveLowerVersion is for when you have a lower version as internal module
	HaveLowerVersion
	//HaveNoVersion is for when you dont have the same module as internal module
	HaveNoVersion
)

// InternalModules that the are integrated into the antibuild binary
var InternalModules = internalModuleMap{}

func (m InternalModule) load() (io.Reader, io.Writer) {
	in, stdin := io.Pipe()
	stdout, out := io.Pipe()

	in2 := bufio.NewReader(in)
	stdout2 := bufio.NewReader(stdout)

	go m.Start(in2, out)

	return stdout2, stdin
}

// MatchesInternalModule checks if a module is available as internal module
// returns an interger indicating the verion available.
func (im *internalModuleMap) Contains(meta Module) int {
	if internal, ok := im.modules[meta.Name]; ok && internal.Repository == meta.Repository {
		if internal.Version.Equals(meta.Version) {
			return HaveSameVersion
		}
		if internal.Version.GT(meta.Version) {
			return HaveHigherVersion
		}
		if internal.Version.LT(meta.Version) {
			return HaveLowerVersion
		}
	}
	return HaveNoVersion
}

func (im *internalModuleMap) Load(meta Module) (io.Reader, io.Writer) {
	if v, ok := im.modules[meta.Name]; ok {
		if meta.Version.Equals(v.Version) {
			stdout2, stdin := v.load()
			return stdout2, stdin
		}
	}
	return nil, nil
}

func (im *internalModuleMap) Set(modules map[string]InternalModule) bool {
	if im.set {
		return false
	}
	im.modules = modules
	im.set = true
	return true
}
