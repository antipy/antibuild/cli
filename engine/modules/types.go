// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"html/template"

	abmc "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/host"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (
	dataWatcher struct {
		host    *host.ModuleHost
		command string
	}

	dataLoader struct {
		host    *host.ModuleHost
		command string
	}

	dataParser struct {
		host    *host.ModuleHost
		command string
	}

	dataPostProcessor struct {
		host    *host.ModuleHost
		command string
	}

	sitePostProcessor struct {
		host    *host.ModuleHost
		command string
	}

	templateFunction struct {
		host    *host.ModuleHost
		command string
	}

	iterator struct {
		host    *host.ModuleHost
		command string
	}

	outputPostProcessor struct {
		host    *host.ModuleHost
		command string
	}
)

/*
	template function
	TODO: make this also return pointer and make an interface for this
*/
func getTemplateFunction(command string, host *host.ModuleHost) templateFunction {
	return templateFunction{
		host:    host,
		command: command,
	}
}

func (tf templateFunction) Run(data ...interface{}) (interface{}, error) {
	var output abmc.TFResponseData
	err := tf.host.ExcecuteMethod("templateFunctions_"+tf.command, data, &output)
	if err != nil {
		return nil, errors.ErrFailedModuleExecution{
			ModuleMethod:  tf.command,
			DetailedError: err,
			ModuleName:    tf.host.Name,
		}
	}
	switch output.Data.(type) {
	case string:
		if output.IsHTMLEscaped {
			output.Data = template.HTML(output.Data.(string))
		} else if output.IsHTMLAttrEscaped {
			output.Data = template.HTMLAttr(output.Data.(string))
		} else if output.IsJSEscaped {
			output.Data = template.JS(output.Data.(string))
		} else if output.IsJSStrEscaped {
			output.Data = template.JSStr(output.Data.(string))
		} else if output.IsCSSEscaped {
			output.Data = template.CSS(output.Data.(string))
		}
	}
	return output.Data, nil
}

/*
	iterators
*/

func getIterator(command string, host *host.ModuleHost) *iterator {
	return &iterator{
		host:    host,
		command: command,
	}
}

func (it *iterator) GetIterations(variable string) ([]string, error) {
	var output []string
	err := it.host.ExcecuteMethod("iterators_"+it.command, variable, &output)
	if err != nil {
		return nil, errors.ErrFailedModuleExecution{
			ModuleMethod:  it.command,
			DetailedError: err,
			ModuleName:    it.host.Name,
		}
	}
	return output, nil
}

/*
	dataWatchers
*/

func getDataWatcher(command string, host *host.ModuleHost) *dataWatcher {
	return &dataWatcher{
		host:    host,
		command: command,
	}
}

func (it *dataWatcher) WatchCallback(variables []string, callback func(i interface{}, e error) bool) error {
	var output string
	err := it.host.ExcecuteMethodAsync("dataWatchers_"+it.command, variables, &output, callback)
	if err != nil {
		return errors.ErrFailedModuleExecution{
			ModuleMethod:  it.command,
			DetailedError: err,
			ModuleName:    it.host.Name,
		}
	}
	return nil
}

/*
	data loaders and post processors
	all of these are pipe-only.
*/
func getDataLoader(command string, host *host.ModuleHost) *dataLoader {
	return &dataLoader{
		host:    host,
		command: command,
	}
}

func (fl *dataLoader) GetPipe(variable string) Pipe {
	pipe := func(binary []byte) ([]byte, error) {
		var data []byte
		err := fl.host.ExcecuteMethod("dataLoaders_"+fl.command, variable, &data)
		if err != nil {
			return nil, errors.ErrModuleFailedToExececutePipe{
				Identifier:    "unknown",
				Version:       "unknown",
				Pipe:          "dataLoaders_" + fl.command,
				DetailedError: err,
			}
		}
		return data, nil
	}
	return pipe
}

func getDataParser(command string, host *host.ModuleHost) *dataParser {
	return &dataParser{
		host:    host,
		command: command,
	}
}

func (fp *dataParser) GetPipe(variable string) Pipe {
	pipe := func(binary []byte) ([]byte, error) {
		if binary == nil { // no daata should not be parsed and just return nil
			return nil, nil
		}
		var data []byte
		err := fp.host.ExcecuteMethod("dataParsers_"+fp.command, variable, &data, binary...)
		if err != nil {
			return nil, errors.ErrModuleFailedToExececutePipe{
				Identifier:    "unknown",
				Version:       "unknown",
				Pipe:          "dataParsers_" + fp.command,
				DetailedError: err,
			}
		}
		return data, nil
	}
	return pipe
}

func getDataPostProcessor(command string, host *host.ModuleHost) *dataPostProcessor {
	return &dataPostProcessor{
		host:    host,
		command: command,
	}
}

func (dpp *dataPostProcessor) GetPipe(variable string) Pipe {
	pipe := func(binary []byte) ([]byte, error) {
		var data []byte
		err := dpp.host.ExcecuteMethod("dataPostProcessors_"+dpp.command, variable, &data, binary...)
		if err != nil {
			return nil, errors.ErrModuleFailedToExececutePipe{
				Identifier:    "unknown",
				Version:       "unknown",
				Pipe:          "dataPostProcessors_" + dpp.command,
				DetailedError: err,
			}
		}
		return data, nil
	}
	return pipe
}

func getSitePostProcessor(command string, host *host.ModuleHost) *sitePostProcessor {
	return &sitePostProcessor{
		host:    host,
		command: command,
	}
}

func (spp *sitePostProcessor) GetPipe(variable string) Pipe {
	pipe := func(binary []byte) ([]byte, error) {
		var data []byte
		err := spp.host.ExcecuteMethod("sitePostProcessors_"+spp.command, variable, &data, binary...)
		if err != nil {
			return nil, errors.ErrModuleFailedToExececutePipe{
				Identifier:    "unknown",
				Version:       "unknown",
				Pipe:          "sitePostProcessors_" + spp.command,
				DetailedError: err,
			}
		}
		return data, nil
	}

	return pipe
}

func getOutputPostProcessor(command string, host *host.ModuleHost) *outputPostProcessor {
	return &outputPostProcessor{
		host:    host,
		command: command,
	}
}

func (spp *outputPostProcessor) Process(input, output string) error {
	err := spp.host.ExcecuteMethod("outputPostProcessors_"+spp.command, []string{input, output}, nil)
	if err != nil {
		return errors.ErrModuleFailedToExececutePipe{
			Identifier:    "unknown",
			Version:       "unknown",
			Pipe:          "outputPostProcessors_" + spp.command,
			DetailedError: err,
		}
	}
	return nil
}
