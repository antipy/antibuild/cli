// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

import (
	"html/template"
	"sync"

	client "gitlab.com/antipy/antibuild/api/client"
	"gitlab.com/antipy/antibuild/api/host"
	"gitlab.com/antipy/antibuild/api/protocol"
	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (

	//ModuleContext stores all the context needed for accesing and loading modules
	ModuleContext struct {
		init              sync.Once
		modules           map[string]*moduleMethods
		templateFunctions template.FuncMap
		BuildMode         string
		logDataLoaders    map[ModuleFunctionID][]string
	}
	moduleMethods struct {
		host               *host.ModuleHost
		module             Module
		init               sync.Once
		Config             map[string]interface{}
		TemplateFunctions  map[string]templateFunction
		DataWatchers       map[string]DataWatcher
		DataLoaders        map[string]DataLoader
		DataParsers        map[string]DataParser
		DataPostProcessors map[string]DPP
		SPPs               map[string]SPP
		Iterators          map[string]Iterator
		OPPs               map[string]OPP
	}
)

//AskMethods asks for the methods that the module can provide
func (mc *ModuleContext) remove(moduleName string) {
	mc.setup()

	mm := mc.modules[moduleName]
	if mm == nil {
		return
	}

	//if some got removed then also remove them
	if len(mm.TemplateFunctions) != 0 {
		mc.updateFuncMap()
	}
	mm.host.Kill()
	delete(mc.modules, moduleName)
}

//RemoveAll simply cleans up all the mess left behind by modules
func (mc *ModuleContext) RemoveAll() {
	if mc == nil {
		return
	}
	mc.setup()
	for _, v := range mc.modules {
		v.host.Kill()
	}
	mc.modules = nil
	mc.updateFuncMap()
}

//AskMethods asks for the methods that the module can provide
func (mc *ModuleContext) AskMethods(moduleName string) (protocol.Methods, error) {
	mc.setup()
	if mc.modules[moduleName].host == nil {
		return nil, nil
	}
	return mc.modules[moduleName].host.AskMethods()
}

//SetMethods sets the module methods for the specified module and methods
func (mc *ModuleContext) SetMethods(moduleName string, methods protocol.Methods) {
	mc.setup()
	mm := mc.modules[moduleName]
	if mm == nil {
		mm = &moduleMethods{}
	}
	//if some got removed then also remove them
	oldLen := len(mm.TemplateFunctions)
	mm.setMethods(methods)
	if len(mm.TemplateFunctions) != 0 || oldLen != 0 {
		mc.updateFuncMap()
	}
	mc.modules[moduleName] = mm
}

//SetHost sets the module host for the specified module using the specified host.
func (mc *ModuleContext) SetHost(host *host.ModuleHost) {
	mc.setup()
	moduleMethods := mc.modules[host.Name]
	moduleMethods.setHost(host)
	if len(moduleMethods.TemplateFunctions) != 0 {
		mc.updateFuncMap()
	}
	mc.modules[host.Name] = moduleMethods
}

//setHost updates all the functions for this module.
func (mm *moduleMethods) setHost(host *host.ModuleHost) {
	mm.host = host
	for id := range mm.TemplateFunctions {
		mm.TemplateFunctions[id] = getTemplateFunction(id, mm.host)
	}

	for id := range mm.DataWatchers {
		mm.DataWatchers[id] = getDataWatcher(id, mm.host)
	}

	for id := range mm.DataLoaders {
		mm.DataLoaders[id] = getDataLoader(id, mm.host)
	}

	for id := range mm.DataParsers {
		mm.DataParsers[id] = getDataParser(id, mm.host)
	}

	for id := range mm.DataPostProcessors {
		mm.DataPostProcessors[id] = getDataPostProcessor(id, mm.host)
	}

	for id := range mm.SPPs {
		mm.SPPs[id] = getSitePostProcessor(id, mm.host)
	}

	for id := range mm.Iterators {
		mm.Iterators[id] = getIterator(id, mm.host)
	}
}

//SetConfig sets the module config for the specified module using the specified config.
//This doesnt actually set the config, that can be done using the FlushConfig method.
func (mc *ModuleContext) SetConfig(moduleName string, config map[string]interface{}) {
	mc.setup()
	mm := mc.modules[moduleName]
	if mm == nil {
		mm = &moduleMethods{}
	}
	mm.Config = config
	mc.modules[moduleName] = mm
}

//FlushConfig flushes the config set for this module to the module.
func (mc *ModuleContext) FlushConfig(moduleName string) error {
	mc.setup()
	moduleMethods := mc.modules[moduleName]
	err := moduleMethods.flushConfig()
	if err != nil {
		return errors.ErrModuleFailedToConfigure{
			Identifier:    moduleName,
			Version:       "unkown",
			DetailedError: err,
		}
	}
	return nil
}

func (mm *moduleMethods) flushConfig() error {
	var output string
	err := mm.host.ExcecuteMethod("internal_config", mm.Config, &output)
	if err != nil || output != client.ModuleReady {
		return err
	}
	return nil
}

func (mm *moduleMethods) setMethods(methods protocol.Methods) {
	mm.TemplateFunctions = make(map[string]templateFunction, len(methods["templateFunctions"]))
	for _, function := range methods["templateFunctions"] {
		mm.TemplateFunctions[function] = getTemplateFunction(function, mm.host)
	}

	mm.DataWatchers = make(map[string]DataWatcher, len(methods["dataWatchers"]))
	for _, function := range methods["dataWatchers"] {
		mm.DataWatchers[function] = getDataWatcher(function, mm.host)
	}

	mm.DataLoaders = make(map[string]DataLoader, len(methods["dataLoaders"]))
	for _, function := range methods["dataLoaders"] {
		mm.DataLoaders[function] = getDataLoader(function, mm.host)
	}

	mm.DataParsers = make(map[string]DataParser, len(methods["dataParsers"]))
	for _, function := range methods["dataParsers"] {
		mm.DataParsers[function] = getDataParser(function, mm.host)
	}

	mm.DataPostProcessors = make(map[string]DPP, len(methods["dataPostProcessors"]))
	for _, function := range methods["dataPostProcessors"] {
		mm.DataPostProcessors[function] = getDataPostProcessor(function, mm.host)
	}

	mm.SPPs = make(map[string]SPP, len(methods["sitePostProcessors"]))
	for _, function := range methods["sitePostProcessors"] {
		mm.SPPs[function] = getSitePostProcessor(function, mm.host)
	}

	mm.Iterators = make(map[string]Iterator, len(methods["iterators"]))
	for _, function := range methods["iterators"] {
		mm.Iterators[function] = getIterator(function, mm.host)
	}
	mm.OPPs = make(map[string]OPP, len(methods["outputPostProcessors"]))
	for _, function := range methods["outputPostProcessors"] {
		mm.OPPs[function] = getOutputPostProcessor(function, mm.host)
	}
}

func (mm *moduleMethods) setup() {
	mm.init.Do(func() {
		if mm.TemplateFunctions == nil {
			mm.TemplateFunctions = make(map[string]templateFunction)
		}
		if mm.DataLoaders == nil {
			mm.DataLoaders = make(map[string]DataLoader)
		}
		if mm.DataParsers == nil {
			mm.DataParsers = make(map[string]DataParser)
		}
		if mm.DataPostProcessors == nil {
			mm.DataPostProcessors = make(map[string]DPP)
		}
		if mm.SPPs == nil {
			mm.SPPs = make(map[string]SPP)
		}
		if mm.Iterators == nil {
			mm.Iterators = make(map[string]Iterator)
		}
		if mm.OPPs == nil {
			mm.OPPs = make(map[string]OPP)
		}
	})
}

func (mc *ModuleContext) setup() {
	mc.init.Do(func() {
		if mc.modules == nil {
			mc.modules = make(map[string]*moduleMethods)
		}
		if mc.templateFunctions == nil {
			mc.templateFunctions = make(template.FuncMap)
		}
	})
}

//GetFuncMap returns the proper functionMap
func (mc *ModuleContext) GetFuncMap() template.FuncMap {
	mc.setup()
	return mc.templateFunctions
}

func (mc *ModuleContext) updateFuncMap() {
	for module, v := range mc.modules {
		for k, fun := range v.TemplateFunctions {
			if k == "" {
				continue
			}
			if module == k {
				mc.templateFunctions[module] = fun.Run
			}
			mc.templateFunctions[module+"_"+k] = fun.Run
		}
	}
}

//GetDataWatcher gets the specified DataWatcher for the specified module
func (mc *ModuleContext) GetDataWatcher(dataLoader ModuleFunctionID) DataWatcher {
	if v, ok := mc.modules[dataLoader.moduleName]; ok {
		return v.getDataWatcher(dataLoader.functionName)
	}
	return nil
}
func (mm *moduleMethods) getDataWatcher(name string) DataWatcher {
	if v, ok := mm.DataWatchers[name]; ok {
		return v
	}
	return nil
}

//GetDataLoader gets the specified DataLoader for the specified module
func (mc *ModuleContext) GetDataLoader(dataLoaderID ModuleFunctionID) DataLoader {
	if v, ok := mc.modules[dataLoaderID.moduleName]; ok {
		dl := v.getDataLoader(dataLoaderID.functionName)
		if mc.logDataLoaders != nil {
			return getPipeWrap(dl, func(variable string, binary []byte) {
				mc.logDataLoaders[dataLoaderID] = append(mc.logDataLoaders[dataLoaderID], variable)
			})
		}

		return dl
	}
	return nil
}

func (mm *moduleMethods) getDataLoader(name string) DataLoader {
	if v, ok := mm.DataLoaders[name]; ok {
		return v
	}
	return nil
}

//GetDataParser gets the specified DataParser for the specified module
func (mc *ModuleContext) GetDataParser(parser ModuleFunctionID) DataParser {
	if v, ok := mc.modules[parser.moduleName]; ok {
		return v.getDataParser(parser.functionName)
	}
	return nil
}

func (mm *moduleMethods) getDataParser(name string) DataParser {
	if v, ok := mm.DataParsers[name]; ok {
		return v
	}
	return nil
}

//GetDataPostProcessor gets the specified DataPostProcessor for the specified module
func (mc *ModuleContext) GetDataPostProcessor(DPP ModuleFunctionID) DPP {
	if v, ok := mc.modules[DPP.moduleName]; ok {
		return v.getDataPostProcessor(DPP.functionName)
	}
	return nil
}

func (mm *moduleMethods) getDataPostProcessor(name string) DPP {
	if v, ok := mm.DataPostProcessors[name]; ok {
		return v
	}
	return nil
}

//GetSPP gets the specified SitePostProcessor for the specified module
func (mc *ModuleContext) GetSPP(SPP ModuleFunctionID) SPP {
	if v, ok := mc.modules[SPP.moduleName]; ok {
		return v.getSPP(SPP.functionName)
	}
	return nil
}

func (mm *moduleMethods) getSPP(name string) SPP {
	if v, ok := mm.SPPs[name]; ok {
		return v
	}
	return nil
}

//GetIterator gets the specified iterator for specified module
func (mc *ModuleContext) GetIterator(iterator ModuleFunctionID) Iterator {
	if v, ok := mc.modules[iterator.moduleName]; ok {
		return v.getIterator(iterator.functionName)
	}
	return nil
}

func (mm *moduleMethods) getIterator(name string) Iterator {
	if v, ok := mm.Iterators[name]; ok {
		return v
	}
	return nil
}

//GetOPP gets the specified OutputPostProcessor for the specified module
func (mc *ModuleContext) GetOPP(OPP ModuleFunctionID) OPP {
	if v, ok := mc.modules[OPP.moduleName]; ok {
		return v.getOPP(OPP.functionName)
	}
	return nil
}

func (mm *moduleMethods) getOPP(name string) OPP {
	if v, ok := mm.OPPs[name]; ok {
		return v
	}
	return nil
}

//InitLog initialises logging of the dataLoaders used
func (mc *ModuleContext) InitLog() {
	mc.logDataLoaders = make(map[ModuleFunctionID][]string)
}

//InitLog initialises logging of the dataLoaders used
func (mc *ModuleContext) StopLog() map[ModuleFunctionID][]string {
	ret := mc.logDataLoaders
	mc.logDataLoaders = nil
	return ret
}
