// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package modules

type (
	pipable interface {
		GetPipe(string) Pipe
	}

	pipeWrap struct {
		callback func(string, []byte)
		child    pipable
	}

	//Pipe is a type used for pipes
	Pipe func([]byte) ([]byte, error)
)

// ExecPipeline is a pipeline executer, data is the input data into the first function,
// retdata is a pointer to where you want the return data to be. Make sure it ais a pointer.
// A pipeline is a set of fuctions that eacht take a file location and process everything based on that.
// For example in modules.go there are .GetPipe methods for all module types, these take the variable
// and return a pipe. At the start of the pipe the provided data is put in, at the end the data is read
// from the same file.
func ExecPipeline(data []byte, pipes ...Pipe) ([]byte, error) {
	for _, pipe := range pipes {
		var err error
		data, err = pipe(data)
		if err != nil {
			return nil, err
		}
	}

	return data, nil
}

func getPipeWrap(pipe pipable, callback func(string, []byte)) *pipeWrap {
	return &pipeWrap{
		callback: callback,
		child:    pipe,
	}
}

func (fl *pipeWrap) GetPipe(variable string) Pipe {
	childPipe := fl.child.GetPipe(variable)
	pipe := func(binary []byte) ([]byte, error) {
		fl.callback(variable, binary)
		data, err := childPipe(binary)
		return data, err
	}
	return pipe
}
