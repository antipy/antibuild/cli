// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package modules is used to manage all the modules connected to antibuild.
// This includes internal modules and modules loaded as external binaries.
package modules

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/antipy/antibuild/cli/internal/errors"

	"gitlab.com/antipy/antibuild/api/host"
)

type (

	//ModuleFunctionID is used for identifying and executing module functions
	ModuleFunctionID struct {
		isContracted bool
		moduleName   string
		functionName string
	}

	//DataWatcher is a module watches for data changes
	DataWatcher interface {
		WatchCallback(variables []string, callback func(i interface{}, e error) bool) error
	}

	//DataLoader is a module that loads data
	DataLoader interface {
		GetPipe(string) Pipe
	}

	//DataParser is a module that parses loaded data
	DataParser interface {
		GetPipe(string) Pipe
	}

	//DPP is a function thats able to post-process data
	DPP interface {
		GetPipe(string) Pipe
	}

	//SPP is a function thats able to post-process data
	SPP interface {
		GetPipe(string) Pipe
	}

	//Iterator is a function thats able to post-process data
	Iterator interface {
		GetIterations(string) ([]string, error)
	}

	//OPP is a function thats able to post-process the output
	OPP interface {
		Process(string, string) error
	}
)

const (
	//StandardRepository is the standard repo for antibuild modules
	StandardRepository = "build.antipy.com/std"
)

// LoadModules loads all modules listed in the deps. Provide nil context if you want a clean slate
func LoadModules(moduleRoot string, modules Modules, context *ModuleContext, log host.Logger) (*ModuleContext, error) {
	deps := modules.Dependencies
	configs := modules.Config
	if context == nil {
		context = &ModuleContext{}
	}

	for _, meta := range deps {
		config := configs[meta.Name]
		if config == nil {
			config = map[string]interface{}{}
		}
		config["antibuild"] = configs["antibuild"]

		context.SetConfig(meta.Name, config)
		//we cant reference meta cuz thats how range works
		err := LoadModule(context, moduleRoot, meta, log)
		if err != nil {
			return nil, err
		}
	}

	return context, nil
}

//LoadModule Loads a specific module and is menth for hotloading, this
//should not be used for initial setup. For initial setup use LoadModules.
func LoadModule(context *ModuleContext, moduleRoot string, meta Module, log host.Logger) error {
	if v, ok := context.modules[meta.Name]; ok {
		if v.module.Repository == meta.Repository && v.module.Version.Equals(meta.Version) {
			return nil
		}
	}

	stdout, stdin, err := loadModule(context, meta, moduleRoot, log)
	if err != nil {
		return err
	}

	host, err := host.Start(stdout, stdin, log, meta.Version.String(), meta.Name)
	if err != nil {
		return err
	}
	context.SetHost(host)

	err = setupModule(context, meta.Name, log)
	if err != nil {
		return err
	}

	context.modules[meta.Name].module = meta
	return nil
}

func loadModule(context *ModuleContext, meta Module, path string, log host.Logger) (io.Reader, io.Writer, error) {
	log.Debugf("Loading module %s from %s at %s version", meta.Name, meta.Repository, meta.Version)
	switch InternalModules.Contains(meta) {
	case HaveSameVersion:
		stdout, stdin := InternalModules.Load(meta)
		if stdout != nil && stdin != nil {
			return stdout, stdin, nil
		}
		err := errors.ErrFailedLoadingModules{
			IsInternal: true,
			Module:     meta.Name,
			Version:    meta.Version,
		}
		log.Warning(err.Error())
	case HaveHigherVersion:
		v := InternalModules.modules[meta.Name]
		if v.Version.Major == meta.Version.Major && meta.Version.Major != 0 {
			log.Infof("Module %s has a more up to date internal version available: %s\n"+
				"This module has the same major version and thus should be compatible", meta.Name, v.Version)
		} else {
			log.Infof("Module %s has a more up to date internal version available: %s", meta.Name, v.Version)
		}
	case HaveLowerVersion, HaveNoVersion: //make sure we only default on something un-implemented
	default:
		log.Warningf("we have something unimplemented in our system. Tell us to implement %s for internal modules.", InternalModules.Contains(meta))
	}

	//prepare command and get nesecary data
	module := exec.Command(filepath.Join(path, "abm_"+meta.Name+"@"+meta.Version.String()))

	stdin, err := module.StdinPipe()
	if nil != err {
		return nil, nil, errors.ErrModuleFailedToObtainConnection{
			Identifier: meta.Name,
			Version:    meta.Version,
			PipeName:   "stdin",
		}
	}

	stdout, err := module.StdoutPipe()
	if nil != err {
		return nil, nil, errors.ErrModuleFailedToObtainConnection{
			Identifier: meta.Name,
			Version:    meta.Version,
			PipeName:   "stdout",
		}
	}

	module.Stderr = os.Stderr

	//start module and initiate connection
	if err := module.Start(); err != nil {
		return nil, nil, errors.ErrModuleFailedToStart{
			Identifier:    meta.Name,
			Version:       meta.Version,
			DetailedError: err,
		}
	}

	return stdout, stdin, nil
}

func setupModule(context *ModuleContext, moduleName string, log host.Logger) error {
	methods, err := context.AskMethods(moduleName)
	if err != nil {
		return errors.ErrModuleFailedToObtainFunctions{
			Identifier:    moduleName,
			Version:       "unkown",
			DetailedError: err,
		}
	}
	log.Debugf("methods for %s: %v", moduleName, methods)
	context.SetMethods(moduleName, methods)
	context.FlushConfig(moduleName)
	return nil
}

func (mf ModuleFunctionID) String() string {
	if mf.isContracted {
		return mf.moduleName
	}
	return mf.moduleName + "_" + mf.functionName
}

//FromBytes converts a byte-sequence to a ModuleFunctionID
func (mf *ModuleFunctionID) FromBytes(data []byte) {
	if data == nil {
		return
	}
	split := bytes.SplitN(data, []byte("_"), 2)

	if len(split) == 1 {
		mf.isContracted = true
		mf.moduleName = string(split[0])
		mf.functionName = string(split[0])
	} else if len(split) == 2 {
		mf.moduleName = string(split[0])
		mf.functionName = string(split[1])
	}
}

//MarshalJSON implements json.Marshaler
func (mf *ModuleFunctionID) MarshalJSON() ([]byte, error) {
	return []byte("\"" + mf.String() + "\""), nil
}

//UnmarshalJSON implements json.Unmarshaler
func (mf *ModuleFunctionID) UnmarshalJSON(data []byte) error {
	data = bytes.TrimPrefix(data, []byte("\""))
	data = bytes.TrimSuffix(data, []byte("\""))
	mf.FromBytes(data)
	return nil
}
