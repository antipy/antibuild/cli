// Copyright © 2018-2019 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

package zip

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// Unzip a zip file into its dstination
func Unzip(src string, dst string) error {

	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	for _, f := range r.File {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer rc.Close()

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dst, f.Name)

		// Check for CVE-2018-8008. AKA ZipSlip
		if !strings.HasPrefix(fpath, filepath.Clean(dst)+string(os.PathSeparator)) {
			return fmt.Errorf("%s: illegal file path", f.Name)
		}
		dirpath := fpath
		if !f.FileInfo().IsDir() {
			dirpath = filepath.Dir(fpath)
		}

		// Make File
		if err = os.MkdirAll(dirpath, os.ModePerm); err != nil {
			return err
		}
		if !f.FileInfo().IsDir() {
			outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			_, err = io.Copy(outFile, rc)
			outFile.Close()
			if err != nil {
				return err
			}

		}

	}
	return nil
}
