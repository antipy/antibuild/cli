// Copyright © 2018-2020 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

//Package net contains all the code for hosting the data and pprof
package net

import (
	"net"
	"net/http"
	"time"

	//pprof should only work when the host is on, otherwise its not gonna be used anyways
	_ "net/http/pprof"
	"os"

	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

var (
	server   http.Server
	shutdown chan int
)

type (
	//Resulter is used for printing the result of building
	Resulter interface {
		Update()
	}
)

//HostDebug host the /debug/pprof endpoint locally on port 5000
func HostDebug() error {
	debug := http.Server{
		Addr:        ":5000",
		Handler:     http.DefaultServeMux,
		ReadTimeout: time.Millisecond * 500,
	}
	err := debug.ListenAndServe()
	if err != nil {
		return err
	}
	return nil
}

//HostLocally hosts output folder
func HostLocally(hostFolder, port string) error {
	if shutdown == nil {
		shutdown = make(chan int, 1)
	} else if len(shutdown) != 0 {
		<-shutdown
	}

	//make sure there is a port set
	addr := ":" + port
	if addr == ":" {
		addr = ":8080"
	}

	//host a static file server from the output folder
	mux := http.NewServeMux()
	mux.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir(hostFolder))))
	server = http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  time.Millisecond * 500,
		WriteTimeout: time.Millisecond * 500,
	}

	//start the server
	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		if sysErr, ok := err.(*net.OpError).Err.(*os.SyscallError); ok && sysErr.Err.Error() == "address already in use" {
			return errors.ErrPortAlreadyInUse{Port: port}
		}
		return errors.ErrHosting{Port: port, DetailedError: err}
	}
	return nil
}
