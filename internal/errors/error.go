// Copyright © 2020 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

// Package errors contains all the errors
package errors

import "github.com/blang/semver"

// ErrFailedOpenFile is when we failed at opening a file
type ErrFailedOpenFile struct {
	FileLocation  string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedOpenFile) Error() string {
	return "Failed to open a file"
}

// --------------

// ErrFailedModuleExecution is when we failed executing a non-pipeline module function
type ErrFailedModuleExecution struct {
	ModuleMethod  string
	DetailedError error
	ModuleName    string
}

// Error method for standard go errors
func (e ErrFailedModuleExecution) Error() string {
	return "failed to execute a module function"
}

// --------------

// ErrFailedCreateFile is when we failed at creating a new file
type ErrFailedCreateFile struct {
	FileLocation  string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedCreateFile) Error() string {
	return "failed to create a file"
}

// --------------

// ErrFailedCreateFolder means that a folder could not be created
type ErrFailedCreateFolder struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedCreateFolder) Error() string {
	return "failed to create folder"
}

// --------------

// ErrFailedRemoveFile is when we failed at removing a file
type ErrFailedRemoveFile struct {
	FileLocation  string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedRemoveFile) Error() string {
	return "failed to remove a file"
}

// --------------

// ErrFileDoesntExist is when we a file doesn't exist
type ErrFileDoesntExist struct {
	FileLocation string
}

// Error method for standard go errors
func (e ErrFileDoesntExist) Error() string {
	return "file doesnt exist"
}

// --------------

// ErrModuleNotExistInternal means the module does not exist as an internal module
type ErrModuleNotExistInternal struct {
	Identifier string
	Version    string
}

// Error method for standard go errors
func (e ErrModuleNotExistInternal) Error() string {
	return "module is not available as an internal module"
}

// --------------

// ErrModuleStringParseFailed means the module string with repo and version could not be parsed
type ErrModuleStringParseFailed struct {
	String string
}

// Error method for standard go errors
func (e ErrModuleStringParseFailed) Error() string {
	return "module string could not be parsed"
}

// --------------

// ErrIteratorStringParseFailed means the iterator string with name and iterator could not be parsed
type ErrIteratorStringParseFailed struct {
	String string
}

// Error method for standard go errors
func (e ErrIteratorStringParseFailed) Error() string {
	return "iterator string could not be parsed"
}

// --------------

// ErrModuleFailedToStart means a module failed to start
type ErrModuleFailedToStart struct {
	Identifier    string
	Version       semver.Version
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleFailedToStart) Error() string {
	return "module failed to start"
}

// --------------

// ErrModuleFailedToObtainConnection means that we could not acquire a connection to the module
type ErrModuleFailedToObtainConnection struct {
	Identifier    string
	Version       semver.Version
	PipeName      string
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleFailedToObtainConnection) Error() string {
	return "failed to obtain the connection pipe to the module"
}

// --------------

//ErrModuleFailedToObtainFunctions means we could not obtain the registered functions
type ErrModuleFailedToObtainFunctions struct {
	Identifier    string
	Version       string
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleFailedToObtainFunctions) Error() string {
	return "the registered functions for the module could not be aquired"
}

// --------------

//ErrModuleFailedToConfigure means we could not configure the module with its configuration
type ErrModuleFailedToConfigure struct {
	Identifier    string
	Version       string
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleFailedToConfigure) Error() string {
	return "module could not be configured with the specified configuration"
}

// --------------

// ErrModuleFailedToExececutePipe in the pipeline
type ErrModuleFailedToExececutePipe struct {
	Identifier    string
	Version       string
	Pipe          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleFailedToExececutePipe) Error() string {
	return "failed to execute pipe in the pipeline"
}

// --------------

// ErrModuleFunctionNotRegistered means that the function that is called is not registered
type ErrModuleFunctionNotRegistered struct {
	Identifier string
	Version    string
	Name       string
}

// Error method for standard go errors
func (e ErrModuleFunctionNotRegistered) Error() string {
	return "means that a function is not registered"
}

// --------------

// ErrModuleDataLoaderNotRegistered that the data loader that is called is not registered
type ErrModuleDataLoaderNotRegistered struct {
	Identifier string
	Version    string
	Name       string
}

// Error method for standard go errors
func (e ErrModuleDataLoaderNotRegistered) Error() string {
	return "means that a data loader is not registered"
}

// --------------

// ErrModuleDataParserNotRegistered that the data parser that is called is not registered
type ErrModuleDataParserNotRegistered struct {
	Identifier string
	Version    string
	Name       string
}

// Error method for standard go errors
func (e ErrModuleDataParserNotRegistered) Error() string {
	return "means that a data parser is not registered"
}

// --------------

// ErrModuleDataPostProcessorNotRegistered that the data post processor that is called is not registered
type ErrModuleDataPostProcessorNotRegistered struct {
	Identifier string
	Version    string
	Name       string
}

// Error method for standard go errors
func (e ErrModuleDataPostProcessorNotRegistered) Error() string {
	return "means that a data post processor is not registered"
}

// --------------

// ErrFailedToParseDataString means that parsing the data string failed
type ErrFailedToParseDataString struct {
	DataString string
	Location   string
}

// Error method for standard go errors
func (e ErrFailedToParseDataString) Error() string {
	return "failed to parse data string"
}

// --------------

// ErrDataStringMissingRangeVariable means that a range variable is missing from a data string
type ErrDataStringMissingRangeVariable struct {
	DataString string
}

// Error method for standard go errors
func (e ErrDataStringMissingRangeVariable) Error() string {
	return "missing range variable in data string"
}

// --------------

// ErrDataStringMissingIterator means that a requested iterator is not defined
type ErrDataStringMissingIterator struct {
	DataString string
	Iterator   string
}

// Error method for standard go errors
func (e ErrDataStringMissingIterator) Error() string {
	return "iterator is used but not defined"
}

// --------------

// ErrFailedCacheInvalidation means that the cache invalidation check failed
type ErrFailedCacheInvalidation struct{}

// Error method for standard go errors
func (e ErrFailedCacheInvalidation) Error() string {
	return "failed cache invalidation check"
}

// --------------

// ErrConfigFailedOpen means the config file could not be opened
type ErrConfigFailedOpen struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrConfigFailedOpen) Error() string {
	return "failed opening the config file"
}

// --------------

// ErrConfigFailedParse means the config file could not be parsed
type ErrConfigFailedParse struct {
	DetailedError error
}

// Error method for standard go errors
func (e ErrConfigFailedParse) Error() string {
	return "failed parsing the config file"
}

// --------------

// ErrConfigFailedWrite means the config file could not be written to
type ErrConfigFailedWrite struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrConfigFailedWrite) Error() string {
	return "failed writing to the config file"
}

// --------------

// ErrNoTemplateFolderSpecified means that the template folder was not specified
type ErrNoTemplateFolderSpecified struct{}

// Error method for standard go errors
func (e ErrNoTemplateFolderSpecified) Error() string {
	return "template folder is not specified"
}

// --------------

// ErrNoOutputFolderSpecified means that the output folder was not specified
type ErrNoOutputFolderSpecified struct{}

// Error method for standard go errors
func (e ErrNoOutputFolderSpecified) Error() string {
	return "output folder is not specified"
}

// --------------

// ErrFailedToCreateLogfile means that the log file could not be created
type ErrFailedToCreateLogfile struct {
	Path          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedToCreateLogfile) Error() string {
	return "could not create log file"
}

// --------------

// ErrFailedToExecuteTemplate means that the execution of the template did not work as expected
type ErrFailedToExecuteTemplate struct {
	OutputPath    string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedToExecuteTemplate) Error() string {
	return "failed to execute template"
}

// --------------

// ErrFailedToMoveStatic means that the static folder could not be moved
type ErrFailedToMoveStatic struct {
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedToMoveStatic) Error() string {
	return "failed to move static folder"
}

// --------------

// ErrUsingUnloadedModule means that the module that is being used is not loaded
type ErrUsingUnloadedModule struct {
	Identifier string
}

// Error method for standard go errors
func (e ErrUsingUnloadedModule) Error() string {
	return "trying to use a unloaded module"
}

// --------------

// ErrFailedToStartFileWatcher means that a file watcher could not be started
type ErrFailedToStartFileWatcher struct {
	Identifier    string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedToStartFileWatcher) Error() string {
	return "file watcher could not be started"
}

// --------------

// ErrFileWatcherCrashed means that a file watcher crashed
type ErrFileWatcherCrashed struct {
	Identifier    string
	DetailedError error
}

// Error method for standard go errors
func (e ErrFileWatcherCrashed) Error() string {
	return "file watcher crashed"
}

// --------------

// ErrFailedToStartKeyboardListener means that a keyboard listener could not be started
type ErrFailedToStartKeyboardListener struct {
	DetailedError error
}

// Error method for standard go errors
func (e ErrFailedToStartKeyboardListener) Error() string {
	return "keyboard listener could not be started"
}

// --------------

// ErrModuleStringParseVersionFailed means that the version could not be parsed
type ErrModuleStringParseVersionFailed struct {
	String        string
	DetailedError error
}

// Error method for standard go errors
func (e ErrModuleStringParseVersionFailed) Error() string {
	return "failed to parse the version"
}

// --------------

// ErrIteratorStringParsing means that a IteratorString has failed to parse
type ErrIteratorStringParsing struct {
	Source        string
	OverPopulated string
}

// Error method for standard go errors
func (e ErrIteratorStringParsing) Error() string {
	return "failed to parse the string"
}

// --------------

// ErrFailedLoadingModules means that the module amodule has failed to load
type ErrFailedLoadingModules struct {
	IsInternal bool
	Module     string
	Version    semver.Version
}

// Error method for standard go errors
func (e ErrFailedLoadingModules) Error() string {
	return "could not parse a module"
}

// --------------

// ErrPortAlreadyInUse means that a port is already being used
type ErrPortAlreadyInUse struct {
	Port string
}

// Error method for standard go errors
func (e ErrPortAlreadyInUse) Error() string {
	return "port is already in use"
}

// --------------

// ErrHosting means that the hosting server encountered a generic error
type ErrHosting struct {
	Port          string
	DetailedError error
}

// Error method for standard go errors
func (e ErrHosting) Error() string {
	return "failed while hosting"
}
