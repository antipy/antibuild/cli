// Copyright © 2020 Antipy V.O.F. info@antipy.com
//
// Licensed under the MIT License

//Package logger contains all the functions for logging
package logger

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"time"

	"gitlab.com/antipy/antibuild/cli/internal/errors"
)

type (
	// MessageType for the log
	MessageType  int
	bufferedFile struct {
		f *os.File
		*bufio.Writer
	}
)

func (mwc *bufferedFile) Close() error {
	if err := mwc.Flush(); err != nil {
		return err
	}
	return mwc.f.Close()
}

const (
	// DEBUG for messages meant for developers and issue submitters
	DEBUG MessageType = iota
	// INFO for user centric information
	INFO
	// WARNING for warnings that might lead into unintended state/errors
	WARNING
	// FATAL for errors that are unrecoverable
	FATAL
)

// Message is a singu;ar log entry
type Message struct {
	Timestamp time.Time
	Type      MessageType
	Message   string
	Error     error
}

// Logger that implements methods to add messages to the message log
type Logger struct {
	Messages       []Message
	MaxMessageType MessageType
	DebugEnabled   bool
	file           io.WriteCloser
}

// New logger
func New(debugEnabled bool, logfile string) *Logger {
	var out io.WriteCloser
	log := Logger{
		Messages:       []Message{},
		MaxMessageType: DEBUG,
		DebugEnabled:   debugEnabled,
	}

	if logfile != "" {
		file, err := os.Create(logfile)
		if err != nil {
			log.Fatale(errors.ErrFailedOpenFile{
				FileLocation:  logfile,
				DetailedError: err,
			})
		}
		out = &bufferedFile{
			file,
			bufio.NewWriter(file),
		}
	}
	log.file = out
	return &log
}

// Debug when something is logged that is primarily used to debug errors
func (l *Logger) Debug(message string) {
	if !l.DebugEnabled {
		return
	}

	l.log(message, DEBUG)
}

// Debuge when something is logged that is primarily used to debug errors - error instead of string
func (l *Logger) Debuge(e error) {
	if !l.DebugEnabled {
		return
	}

	l.logE(e, DEBUG)
}

// Debugf when something is logged that is primarily used to debug errors - with formatter
func (l *Logger) Debugf(format string, a ...interface{}) {
	if !l.DebugEnabled {
		return
	}
	l.Debug(fmt.Sprintf(format, a...))
}

// Info when something should be shown to the user
func (l *Logger) Info(message string) {
	l.log(message, INFO)
}

// Infoe when something should be shown to the user - error instead of string
func (l *Logger) Infoe(e error) {
	l.logE(e, INFO)
}

// Infof when something should be shown to the user - with formatter
func (l *Logger) Infof(format string, a ...interface{}) {
	l.Info(fmt.Sprintf(format, a...))
}

// Errore when something errors that is recoverable - error instead of string
func (l *Logger) Errore(e error) {
	l.logE(e, WARNING)
}

// Errorf when something errors that is recoverable - with formatter
func (l *Logger) Errorf(format string, a ...interface{}) {
	l.Error(fmt.Sprintf(format, a...))
}

// Error when something errors that is recoverable
func (l *Logger) Error(message string) {
	l.log(message, WARNING)
}

// Warning when something happpens that might lead to undesirable state
func (l *Logger) Warning(message string) {
	l.log(message, WARNING)
}

// Warninge when something happpens that might lead to undesirable state - error instead of string
func (l *Logger) Warninge(e error) {
	l.logE(e, WARNING)
}

// Warningf when something happpens that might lead to undesirable state - with formatter
func (l *Logger) Warningf(format string, a ...interface{}) {
	l.Warning(fmt.Sprintf(format, a...))
}

// Fatal when something errors that is unrecoverable
func (l *Logger) Fatal(message string) {
	l.log(message, FATAL)
}

// Fatale when something errors that is unrecoverable - error instead of string
func (l *Logger) Fatale(e error) {
	l.logE(e, FATAL)
}

// Fatalf when something errors that is unrecoverable  - with formatter
func (l *Logger) Fatalf(format string, a ...interface{}) {
	l.Fatal(fmt.Sprintf(format, a...))
}

func (l *Logger) log(message string, messageType MessageType) {
	if l.MaxMessageType < messageType {
		l.MaxMessageType = messageType
	}

	l.Messages = append(l.Messages, Message{
		Timestamp: time.Now(),
		Type:      messageType,
		Message:   message,
	})

	if l.file != nil {
		fmt.Fprintf(l.file, "[%d] %s\n", messageType, message)
	}
}

func (l *Logger) logE(err error, messageType MessageType) {
	if l.MaxMessageType < messageType {
		l.MaxMessageType = messageType
	}

	l.Messages = append(l.Messages, Message{
		Timestamp: time.Now(),
		Type:      messageType,
		Message:   err.Error(),
		Error:     err,
	})

	if l.file != nil {
		fmt.Fprintf(l.file, "[%d] %s\n", messageType, err.Error())
	}
}

// Close is menth to flush the logs to any potential file output
func (l *Logger) Close() error {
	if l.file != nil {
		return l.file.Close()
	}
	return nil
}
